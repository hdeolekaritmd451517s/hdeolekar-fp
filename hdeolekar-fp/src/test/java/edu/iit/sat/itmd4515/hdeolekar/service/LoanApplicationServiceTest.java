/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.iit.sat.itmd4515.hdeolekar.dao.LoanApplicationDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.Address;
import edu.iit.sat.itmd4515.hdeolekar.model.CoBorrower;
import edu.iit.sat.itmd4515.hdeolekar.model.Collateral;
import edu.iit.sat.itmd4515.hdeolekar.model.CollateralType;
import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.model.EmploymentType;
import edu.iit.sat.itmd4515.hdeolekar.model.Gender;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanApplication;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanType;

/**
 * This class is used to test loan application service.
 * @author Harshal
 * @project hdeolekar-lab5
 * @since Feb 13, 2017
 */
public class LoanApplicationServiceTest {
	static LoanApplicationService loanApplicationService;
	static List<LoanApplication> listOfLoanApplication;
	static SimpleDateFormat sf = new SimpleDateFormat("M-dd-yyyy");
	LoanApplication loanApp;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		loanApplicationService = new LoanApplicationService();
		listOfLoanApplication = new ArrayList<LoanApplication>();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		///JPASEEntityManager.close();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Address chicagoCustomerAddress = new Address("450 West 34th St", "Apt 708", "Chicago", "60616", "Illinois",
				"United States");
		Customer chicagoCustomer = new Customer("Presza", "Hudson", "145255699", Gender.FEMALE, sf.parse("10-25-1992"),
				"5129746968", "hdeolekar@hawk.iit.edu", chicagoCustomerAddress);

		/*Address texasCustomerAddress = new Address("2500 Broadway", "", "Lubbock",  "60616", "Texas",
				"United States");*/
		/*Customer texasCustomer = new Customer("Jean", "Hubrath", "145247997", Gender.FEMALE, sf.parse("10-15-1998"),
				"5056746968", "hdeolekar@hawk.iit.edu", texasCustomerAddress);*/

		loanApp = new LoanApplication();
		loanApp.setEmail("test@example.com");
		loanApp.setMonthlyIncome(new BigDecimal(28000));
		loanApp.setPhone("7739759451");
		loanApp.setLoanAmount(new BigDecimal(145000));
		loanApp.setLoanType(LoanType.CAR_LOAN);
		loanApp.setLoanApplicationDate(new Date());
		loanApp.setCustomer(chicagoCustomer);

		CoBorrower coborrower1 = new CoBorrower("Demy", "Morock", "Sister", new BigDecimal(20000), EmploymentType.SELF_EMPLOYED_BUSINESS);
		CoBorrower coborrower2 = new CoBorrower("Nancy", "Morock", "Sister", new BigDecimal(35000), EmploymentType.SALARIED);
		loanApp.getCoborrower().add(coborrower1);
		loanApp.getCoborrower().add(coborrower2);

		Collateral collateral = new Collateral(CollateralType.HOUSE, new BigDecimal(450000));
		loanApp.setCollateral(collateral);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		loanApp = null;
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.LoanApplicationService#getAllLoanApplication()}.
	 */
	@Test
	public void testGetAllLoanApplication() {
		//loanApplicationService.submitLoanApplication(loanApp);
		System.out.println("Customer Id: "+loanApp.getCustomer().getId());
		List<LoanApplication> loanApp2 = loanApplicationService.getAllApprovedLoanApplication();
		Assert.assertNotNull("List Must not be null", loanApp2);

	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.LoanApplicationService#getLoanApplicationById(long)}.
	 */
	@Test
	public void testGetLoanApplicationById() {
		loanApp.setLoanAmount(new BigDecimal(25000));
		//loanApplicationService.submitLoanApplication(loanApp);
		System.out.println("Loan Application Id: "+loanApp.getId());

		/*LoanApplication loanApplication2 = loanApplicationService.getLoanApplicationById(loanApp.getId());
		Assert.assertEquals(loanApp.getLoanAmount().intValue(), loanApplication2.getLoanAmount().intValue());

		Assert.assertNotNull("LoanApplication must not be null", loanApplication2);
		Assert.assertNotNull("Customer must not be null", loanApplication2.getCustomer());
		Assert.assertNotNull("Collateral must not be null", loanApplication2.getCollateral());
		Assert.assertNotNull("Collateral must not be null", loanApplication2.getCoborrower());
		Assert.assertEquals(loanApp.getCoborrower().size(), loanApplication2.getCoborrower().size());*/


	}


	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.LoanApplicationService#getLoanApplicationByCustomerId(long)}.
	 */
	@Test
	public void testGetLoanApplicationByCustomerId() {
		loanApp.setLoanAmount(new BigDecimal(60000));
		//loanApplicationService.submitLoanApplication(loanApp);
		System.out.println("Loan Application Customer Id: "+loanApp.getCustomer().getId());

		/*LoanApplication loanApplication3 = loanApplicationService.getLoanApplicationById(loanApp.getCustomer().getId());
		Assert.assertEquals(loanApp.getLoanAmount().intValue(), loanApplication3.getLoanAmount().intValue());*/
	}


	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.LoanApplicationService#submitLoanApplication(edu.iit.sat.itmd4515.hdeolekar.model.LoanApplication)}.
	 */
	@Test
	public void testSubmitLoanApplication() {
		loanApp.setEmail("hdeolekar@hawk.iit.edu");
		//loanApplicationService.submitLoanApplication(loanApp);
		Assert.assertTrue(loanApp.getId() > 0);
		/*LoanApplication loanApplication2 = loanApplicationService.getLoanApplicationById(loanApp.getId());
		Assert.assertEquals(loanApp.getEmail(), loanApplication2.getEmail());*/
	}


	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.LoanApplicationService#deleteLoanApplication(edu.iit.sat.itmd4515.hdeolekar.model.LoanApplication)}.
	 */
	@Test
	public void testDeleteLoanApplication() {
		loanApp.setLoanType(LoanType.CAR_LOAN);
		//loanApplicationService.submitLoanApplication(loanApp);
		System.out.println(loanApp.getId());
		loanApplicationService.deleteLoanApplication(loanApp);
		Assert.assertNull("Loan Deleted - Must be null",loanApplicationService.getLoanApplicationById(loanApp.getId()));
	}

}
