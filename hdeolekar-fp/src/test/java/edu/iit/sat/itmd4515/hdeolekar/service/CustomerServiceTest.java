/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.service;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.iit.sat.itmd4515.hdeolekar.model.Address;
import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.model.Gender;

/**
 * This class is used to test customer service.
 * @author Harshal
 * @project hdeolekar-lab5
 * @since Feb 12, 2017
 */
public class CustomerServiceTest {
	private Validator validator;
	@EJB
	static CustomerService customerService;
	static List<Customer> listOfCustomers = null;
	SimpleDateFormat sf = new SimpleDateFormat("M-dd-yyyy");
	static int count = 0;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static  void setUpBeforeClass() throws Exception {
		/*customerService = new CustomerService(new CustomerDAO());*/
		listOfCustomers = new ArrayList<Customer>();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static  void tearDownAfterClass() throws Exception {
		//JPASEEntityManager.close();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
		Address chicagoCustomerAddress = new Address("450 West 34th St", "Apt 708", "Chicago", "60616", "Illinois",
				"United States");
		Customer chicagoCustomer = new Customer("Presza", "Hudson", "145255699", Gender.FEMALE, sf.parse("10-25-1992"),
				"5129746968", "hdeolekar@hawk.iit.edu", chicagoCustomerAddress);

		Address texasCustomerAddress = new Address("2500 Broadway", "", "Lubbock",  "60616", "Texas",
				"United States");
		Customer texasCustomer = new Customer("Jean", "Hubrath", "145247997", Gender.FEMALE, sf.parse("10-15-1998"),
				"5056746968", "hdeolekar@hawk.iit.edu", texasCustomerAddress);
		listOfCustomers.add(chicagoCustomer);
		listOfCustomers.add(texasCustomer);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		validator = null;
	}

	/**
	 * Bean Validation for Date of Birth
	 */
	@Test
	public void testCustomerPastDateOfBirth() {
		try {
			System.out.println("Date Of Birth Test Case");
			Customer customer = listOfCustomers.get(count);
			Set<ConstraintViolation<Customer>> violations = validator.validate(customer);
			 for(ConstraintViolation<Customer> violation : violations){
		            System.out.println("Violation DOB: "+violation.toString());
		        }
	        assertTrue(violations.isEmpty());
			//customerService.registerCustomer(customer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Bean Validation Test Case: Test case for invalid social security number.
	 */
	@Test
	public void testCustomerSocialSecurityNumberTest() {
		try {
			System.out.println(count+": Registering Customer");
			Customer customer = listOfCustomers.get(count);
			customer.setSocialSecurityNumber("1234567890");
			Set<ConstraintViolation<Customer>> violations = validator.validate(customer);
			 for(ConstraintViolation<Customer> violation : violations){
		            System.out.println("Social Security Number "+violation.toString());
		      }
	        assertFalse(violations.isEmpty());
	        customer.setSocialSecurityNumber("123456789");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This test cases validates invalid phone number (Bean Validation)
	 */
	@Test
	public void testCustomerPhoneNumberTest() {
		count += 1;
		System.out.println("Phone Number Test Case");
		Customer customer = listOfCustomers.get(count);
		 Set<ConstraintViolation<Customer>> violations = validator.validate(customer);

        for(ConstraintViolation<Customer> violation : violations){
            System.out.println(violation.toString());
        }

        assertTrue(violations.isEmpty());
	}

	@Test
	public void testGetAllCustomers() {
		System.out.println(count+": Get all customers");
		List<Customer> customerList = customerService.getAllCustomers();
		Assert.assertNotNull("Customer object must not be null", customerList);
		customerList.forEach(c -> Assert.assertNotNull(c));
	}


}
