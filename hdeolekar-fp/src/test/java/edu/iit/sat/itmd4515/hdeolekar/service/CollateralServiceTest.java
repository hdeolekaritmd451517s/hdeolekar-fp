/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.iit.sat.itmd4515.hdeolekar.dao.CollateralDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.Collateral;
import edu.iit.sat.itmd4515.hdeolekar.model.CollateralType;

/**
 * This class is used to test collateral service.
 * @author Harshal
 * @project hdeolekar-lab5
 * @since Feb 13, 2017
 */
public class CollateralServiceTest {
	static CollateralService collateralService;
	static List<Collateral> listOfCollateral = null;
	static int count = 0;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		collateralService = new CollateralService(new CollateralDAO());
		listOfCollateral = new ArrayList<Collateral>();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		//JPASEEntityManager.close();
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.CollateralService#registerCollateral(edu.iit.sat.itmd4515.hdeolekar.model.Collateral)}.
	 */
	@Test
	public void testRegisterCollateral() {
		Collateral collateral = new Collateral(CollateralType.HOUSE, new BigDecimal(470000));
		Assert.assertNotNull("Collateral object must not be null", collateral);
		collateralService.registerCollateral(collateral);
		Collateral collateral1 = collateralService.getCollateralById(collateral.getId());
		Assert.assertNotNull("Collateral object must not be null", collateral1);
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.CollateralService#deleteCollateral(edu.iit.sat.itmd4515.hdeolekar.model.Collateral)}.
	 */
	@Test
	public void testDeleteCollateral() {
		Collateral collateral = new Collateral(CollateralType.HOUSE, new BigDecimal(470000));
		collateralService.registerCollateral(collateral);
		Collateral collateral2 = collateralService.getCollateralById(collateral.getId());
		assertEquals(collateral.getId(), collateral2.getId());
		Assert.assertNotNull("Collateral object must not be null", collateral);
		collateralService.deleteCollateral(collateral);
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.CollateralService#getAllCollateral()}.
	 */
	@Test
	public void testGetAllCollateral() {
		Collateral collateral = new Collateral(CollateralType.HOUSE, new BigDecimal(55000));
		collateralService.registerCollateral(collateral);
		List<Collateral> collateralList = collateralService.getAllCollateral();
		Assert.assertNotNull("Collateral object must not be null", collateralList);
		collateralList.forEach(c -> Assert.assertNotNull(c));
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.CollateralService#getCollateralById(long)}.
	 */
	@Test
	public void testGetCollateralById() {
		Collateral collateral = new Collateral(CollateralType.HOUSE, new BigDecimal(55000));
		Assert.assertNotNull("Collateral object must not be null", collateral);
		collateralService.registerCollateral(collateral);
		Collateral collateral1 = collateralService.getCollateralById(collateral.getId());
		Assert.assertNotNull("Collateral object must not be null", collateral1);
	}
}
