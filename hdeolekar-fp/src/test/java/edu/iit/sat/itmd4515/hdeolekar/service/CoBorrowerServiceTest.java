/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.iit.sat.itmd4515.hdeolekar.dao.CoBorrowerDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.CoBorrower;
import edu.iit.sat.itmd4515.hdeolekar.model.EmploymentType;

/**
 * This class is used to test co-borrower service.
 * @author Harshal
 * @project hdeolekar-lab5
 * @since Feb 13, 2017
 */
public class CoBorrowerServiceTest {
	static CoBorrowerService coBorrowerService;
	static List<CoBorrower> listOfCoBorrowers = null;
	static int count = 0;
	CoBorrower coborrower1;
	CoBorrower coborrower2;


	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		coBorrowerService = new CoBorrowerService(new CoBorrowerDAO());
		listOfCoBorrowers = new ArrayList<CoBorrower>();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		//JPASEEntityManager.close();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		coborrower1 = new CoBorrower("Jean", "Hudson", "Sister", new BigDecimal(20000), EmploymentType.SELF_EMPLOYED_BUSINESS);
		coborrower2 = new CoBorrower("Matt", "Johnson", "Brother", new BigDecimal(10000), EmploymentType.SELF_EMPLOYED_PROFESSIONAL);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		coborrower1 = null;
		coborrower2 = null;

	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.CoborrowerService#registerCoborrower(edu.iit.sat.itmd4515.hdeolekar.model.CoBorrower)}.
	 */
	@Test
	public void testRegisterCoborrower() {
		Assert.assertNotNull("Coborrower object must not be null", coborrower1);
		coBorrowerService.registerCoborrower(coborrower1);
		CoBorrower coBorrower2 = coBorrowerService.getCoBorrowerById(coborrower1.getId());
		Assert.assertNotNull("Collateral object must not be null", coBorrower2);
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.CoborrowerService#deleteCoborrower(edu.iit.sat.itmd4515.hdeolekar.model.CoBorrower)}.
	 */
	@Test
	public void testDeleteCoborrower() {
		coBorrowerService.registerCoborrower(coborrower1);
		coBorrowerService.deleteCoborrower(coborrower1);
		CoBorrower coBorrower2 = coBorrowerService.getCoBorrowerById(coborrower1.getId());
		Assert.assertNull("Collateral object must not be null", coBorrower2);
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.CoborrowerService#getAllCoBorrower()}.
	 */
	@Test
	public void testGetAllCoBorrower() {
		coBorrowerService.registerCoborrower(coborrower1);
		List<CoBorrower> coBorrowerList = coBorrowerService.getAllCoBorrower();
		Assert.assertNotNull("CoBorrower object must not be null", coBorrowerList);
		coBorrowerList.forEach(c -> Assert.assertNotNull(c));
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.CoborrowerService#getCoBorrowerById(long)}.
	 */
	@Test
	public void testGetCoBorrowerById() {
		coBorrowerService.registerCoborrower(coborrower2);
		CoBorrower coBorrower2 = coBorrowerService.getCoBorrowerById(coborrower2.getId());
		Assert.assertNotNull("CoBorrower object must not be null", coBorrower2);
		Assert.assertTrue(coBorrower2.getId() > 1);
	}
}
