package edu.iit.sat.itmd4515.hdeolekar.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.iit.sat.itmd4515.hdeolekar.dao.LoanDetailDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.Address;
import edu.iit.sat.itmd4515.hdeolekar.model.CoBorrower;
import edu.iit.sat.itmd4515.hdeolekar.model.Collateral;
import edu.iit.sat.itmd4515.hdeolekar.model.CollateralType;
import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.model.EmploymentType;
import edu.iit.sat.itmd4515.hdeolekar.model.Gender;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanApplication;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanDetails;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanType;

/**
 * This class is used to test loan details service.
 * @author Harshal
 * @project hdeolekar-lab5
 * @since Feb 13, 2017
 */
public class LoanDetailServiceTest {
	static LoanDetailService loanDetailService;
	static List<LoanDetails> listOfLoanDetails;
	SimpleDateFormat sf = new SimpleDateFormat("M-dd-yyyy");
	LoanDetails loanDetails;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static  void setUpBeforeClass() throws Exception {
		loanDetailService = new LoanDetailService(new LoanDetailDAO());
		listOfLoanDetails = new ArrayList<LoanDetails>();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static  void tearDownAfterClass() throws Exception {
	//	JPASEEntityManager.close();
	}

	@Before
	public void setUp() throws Exception {
		Address chicagoCustomerAddress = new Address("450 West 34th St", "Apt 708", "Chicago", "60616", "Illinois",
				"United States");
		Customer chicagoCustomer = new Customer("Presza", "Hudson", "145255699", Gender.FEMALE, sf.parse("10-25-1992"),
				"5129746968", "hdeolekar@hawk.iit.edu", chicagoCustomerAddress);

/*		Address texasCustomerAddress = new Address("2500 Broadway", "", "Lubbock",  "60616", "Texas",
				"United States");*/
		/*Customer texasCustomer = new Customer("Jean", "Hubrath", "145247997", Gender.FEMALE, sf.parse("10-15-1998"),
				"5056746968", "hdeolekar@hawk.iit.edu", texasCustomerAddress);*/
		LoanApplication loanApp = new LoanApplication();
		loanApp.setEmail("test@example.com");
		loanApp.setMonthlyIncome(new BigDecimal(28000));
		loanApp.setPhone("7739759451");
		loanApp.setLoanAmount(new BigDecimal(145000));
		loanApp.setLoanType(LoanType.CAR_LOAN);
		loanApp.setLoanApplicationDate(new Date());
		loanApp.setCustomer(chicagoCustomer);

		CoBorrower coborrower1 = new CoBorrower("Demy", "Morock", "Sister", new BigDecimal(20000), EmploymentType.SELF_EMPLOYED_BUSINESS);
		CoBorrower coborrower2 = new CoBorrower("Nancy", "Morock", "Sister", new BigDecimal(35000), EmploymentType.SALARIED);
		loanApp.getCoborrower().add(coborrower1);
		loanApp.getCoborrower().add(coborrower2);

		Collateral collateral = new Collateral(CollateralType.HOUSE, new BigDecimal(450000));
		loanApp.setCollateral(collateral);

		loanDetails = new LoanDetails();
		loanDetails.setLoanAmount(loanApp.getLoanAmount());
		loanDetails.setLoanApplicationDate(loanApp.getLoanApplicationDate());
		loanDetails.setLoanSanctionDate(new Date());
		loanDetails.setLoanTenure(7);
		loanDetails.setLoanType(loanApp.getLoanType());
		loanDetails.setCustomer(loanApp.getCustomer());
		loanDetails.getCoborrowers().addAll(loanApp.getCoborrower());
		loanDetails.setCollateral(loanApp.getCollateral());
	}

	@After
	public void tearDown() throws Exception {
		loanDetails = null;
	}

	@Test
	public void testSubmitLoanDetails(){
		loanDetails.setLoanTenure(3);
		loanDetailService.submitLoanDetails(loanDetails);
		System.out.println(loanDetails.getId());
		Assert.assertTrue(loanDetails.getLoanAmount().intValue() > 1000);
	}

	@Test
	public void testDeleteLoanDetails(){
		loanDetails.setLoanType(LoanType.CAR_LOAN);
		loanDetailService.submitLoanDetails(loanDetails);
		System.out.println(loanDetails.getId());
		loanDetailService.deleteLoanDetails(loanDetails);
		Assert.assertNull("Loan Deleted - Must be null",loanDetailService.getLoanDetailsById(loanDetails.getId()));
	}

	@Test
	public void testGetLoanDetailsByCustomerId() {
		loanDetailService.submitLoanDetails(loanDetails);

		System.out.println("Customer Id: "+loanDetails.getCustomer().getId());
		LoanDetails loanDetails2 = loanDetailService.getLoanDetailsByCustomerId(loanDetails.getCustomer().getId());
		Assert.assertEquals(loanDetails.getId(), loanDetails2.getId());
	}

	@Test
	public void testGetLoanDetailsById() {
		loanDetails.setLoanTenure(15);
		loanDetailService.submitLoanDetails(loanDetails);
		System.out.println("LoanDetails ID: "+loanDetails.getId());

		LoanDetails loanDetails2 = loanDetailService.getLoanDetailsById(loanDetails.getId());
		Assert.assertEquals(loanDetails.getLoanTenure(), loanDetails2.getLoanTenure());
	}

	@Test
	public void testGetLoanEMI() {
		loanDetails.setLoanAmount(new BigDecimal(75000));
		loanDetails.setLoanTenure(15);
		loanDetails.setInterestRate(new BigDecimal(12));
		loanDetailService.submitLoanDetails(loanDetails);
		System.out.println("LoanDetails ID: "+loanDetails.getId());

		BigDecimal loanEMIAmount = loanDetailService.getLoanEMIPayment(loanDetails.getId());
		System.out.println("For loan of amount "+loanDetails.getLoanAmount()+" sanctioned for "+loanDetails.getLoanTenure()+
				"years, number of payments to be made are "+loanDetailService.getNumberOfInstallments(loanDetails.getId())+
				". EMI amount per month would be $"+loanEMIAmount.floatValue());
		System.out.println("Total repayment for loan of $" + loanDetails.getLoanAmount() + " with interest rate of "
				+ loanDetails.getInterestRate() + "% would be $"
				+ (loanDetailService.getNumberOfInstallments(loanDetails.getId()) * loanEMIAmount.floatValue()));
		Assert.assertFalse(loanEMIAmount == null ||loanEMIAmount.intValue() < 0 || loanEMIAmount.intValue() == 0);
		Assert.assertTrue(loanEMIAmount.intValue() > 0);
	}

	@Test
	public void testGetNumberOfInstallments(){
		loanDetails.setLoanAmount(new BigDecimal(50000));
		loanDetails.setLoanTenure(4);
		loanDetails.setInterestRate(new BigDecimal(9.50));
		loanDetailService.submitLoanDetails(loanDetails);
		System.out.println("LoanDetails ID: "+loanDetails.getId());

		int numberOfInstallments = loanDetailService.getNumberOfInstallments(loanDetails.getId());
		Assert.assertTrue(numberOfInstallments > 0);
	}

	@Test
	public void testGetLoanTenure(){
		loanDetails.setLoanAmount(new BigDecimal(32000));
		loanDetails.setLoanTenure(6);
		loanDetails.setInterestRate(new BigDecimal(7));
		loanDetailService.submitLoanDetails(loanDetails);
		System.out.println("LoanDetails ID: "+loanDetails.getId());

		int loanTenure = loanDetailService.getLoanTenure(loanDetails.getId());
		Assert.assertTrue(loanTenure == 6);
	}

	@Test
	public void testGetAllLoanDetails() {
		loanDetailService.submitLoanDetails(loanDetails);
		System.out.println("Customer Id: "+loanDetails.getCustomer().getId());
		List<LoanDetails> loanDetails2 = loanDetailService.getAllLoanApplicationDetails();
		Assert.assertNotNull("List Must not be null", loanDetails2);
	}
}
