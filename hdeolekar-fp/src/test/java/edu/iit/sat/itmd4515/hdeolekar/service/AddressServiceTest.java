package edu.iit.sat.itmd4515.hdeolekar.service;
/**
 *
 */


import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.iit.sat.itmd4515.hdeolekar.dao.AddressDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.Address;

/**
 * This class is used to test address service.
 * @author Harshal
 * @project hdeolekar-lab5
 * @since Feb 13, 2017
 */
public class AddressServiceTest {
	static AddressService addressService;
	static List<Address> listOfAddress = null;
	SimpleDateFormat sf = new SimpleDateFormat("M-dd-yyyy");
	static int count = 0;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		addressService = new AddressService(new AddressDAO());
		listOfAddress = new ArrayList<Address>();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		//JPASEEntityManager.close();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Address chicagoCustomerAddress = new Address("450 West 34th St", "Apt 708", "Chicago", "60616", "Illinois",
				"United States");
		/*Customer chicagoCustomer = new Customer("Presza", "Hudson", "145255699", Gender.FEMALE, sf.parse("10-25-1992"),
				"5129746968", "hdeolekar@hawk.iit.edu", chicagoCustomerAddress);
*/
		Address texasCustomerAddress = new Address("2500 Broadway", "", "Lubbock",  "60616", "Texas",
				"United States");
	/*	Customer texasCustomer = new Customer("Jean", "Hubrath", "145247997", Gender.FEMALE, sf.parse("10-15-1998"),
				"5056746968", "hdeolekar@hawk.iit.edu", texasCustomerAddress);*/
		listOfAddress.add(texasCustomerAddress);
		listOfAddress.add(chicagoCustomerAddress);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.AddressService#registerAddress(edu.iit.sat.itmd4515.hdeolekar.model.Address)}.
	 */
	@Test
	public void testRegisterAddress() {
		try {
			count += 1;
			System.out.println(count+": Registering Address");
			Address address = listOfAddress.get(count);
			Assert.assertNotNull("Address object must not be null", address);

			//Bean Validation --
			Assert.assertEquals(0, address.getId());
			addressService.registerAddress(address);
			Assert.assertNotEquals(0, address.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.AddressService#deleteAddress(edu.iit.sat.itmd4515.hdeolekar.model.Address)}.
	 */
	@Test
	public void testDeleteAddress() {
		System.out.println(count+": Delete Address");
		Address address = addressService.getAddressById(count);
		assertEquals(count, address.getId());
		Assert.assertNotNull("Address object must not be null", address);
		addressService.deleteAddress(address);
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.AddressService#getAllAddress()}.
	 */
	@Test
	public void testGetAllAddress() {
		System.out.println(count+": Get all Address");
		List<Address> addressList = addressService.getAllAddress();
		Assert.assertNotNull("Address object must not be null", addressList);
		addressList.forEach(c -> Assert.assertNotNull(c));
	}

	/**
	 * Test method for {@link edu.iit.sat.itmd4515.hdeolekar.service.AddressService#getAddressById(long)}.
	 */
	@Test
	public void testGetAddressById() {
		count += 1;
		System.out.println(count+": Get Address by ID");
		Address address1 = listOfAddress.get(count);
		Assert.assertNotNull("Address object must not be null", address1);
		addressService.registerAddress(address1);

		Address address2 = addressService.getAddressById(count);
		System.out.println(address2);
		Assert.assertNotNull("Address object must not be null", address2);
		assertEquals(count, address2.getId());
	}
}
