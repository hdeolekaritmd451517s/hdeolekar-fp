<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page errorPage="error.jsp"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Success</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Alegreya|Slabo+27px"
	rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
tbody {
	background-color: #B30753;
}

table th, td {
	text-align: center;
	color: black;
	background-color: #ccc;
	width: 80%;
}

#results, h3 {
	font-family: 'Alegreya', serif;
	max-width: 80%;
	margin: auto;
	text-transform: uppercase;
	margin-top: 20px;
	border: 0px solid #34495E;
}

tr {
	padding:10px;
}
body {
	background-color: #FAFAFA;
}
</style>
</head>
<body>
<body>
	<h3 style="text-align: center; padding-top: 38px; height: 100px; color: white; background-color: #476268; border-top-left-radius: 5px; border-top-right-radius: 5px;">
	!Registration Successful</h3>
	<div id="results" class="table-responsive">
		<table>
			<thead>
				<tr>
					<th>Customer Details</th>
					<th>Results</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>First Name</td>
					<td>${requestScope.customer.firstName}</td>
				</tr>
				<tr>
					<td>Last Name</td>
					<td>${requestScope.customer.lastName}</td>
				</tr>
				<tr>
					<td>Social Security Number</td>
					<td>${requestScope.customer.socialSecurityNumber}</td>
				</tr>
				<tr>
					<td>Gender</td>
					<td>${requestScope.customer.gender}</td>
				</tr>
				<tr>
					<td>Email</td>
					<td>${requestScope.customer.email}</td>
				</tr>
				<tr>
					<td>Phone</td>
					<td>${requestScope.customer.phone}</td>
				</tr>
				<tr>
					<td>Date Of Birth</td>
					<td>${requestScope.customer.dateOfBirth}</td>
				</tr>
				<tr>
					<th>Address Details</th>
					<th>Results</th>
				</tr>
				<tr>
					<td>Address 1</td>
					<td>${requestScope.address.address1}</td>
				</tr>
				<tr>
					<td>Address 2</td>
					<td>${requestScope.address.address2}</td>
				</tr>
				<tr>
					<td>City</td>
					<td>${requestScope.address.city}</td>
				</tr>
				<tr>
					<td>Pin Code</td>
					<td>${requestScope.address.pincode}</td>
				</tr>
				<tr>
					<td>State</td>
					<td>${requestScope.address.state}</td>
				</tr>
				<tr>
					<td>Country</td>
					<td>${requestScope.address.country}</td>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>