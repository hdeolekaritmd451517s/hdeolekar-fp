<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="https://fonts.googleapis.com/css?family=Alegreya|Slabo+27px" rel="stylesheet">
<title>Error!</title>
<style type="text/css">
	body {
		background-color: #34495e;
		font-family: 'Alegreya', serif;
		font-size: 18px;
	}
	table {
		width: 50%;
		margin: auto;
		border-radius: 6px;
		color: red;
		border: 3px solid #bdc3c7;
		background-color: #ecf0f1;
	}
	 h3 {
	 	width: 50%;
		margin: auto;
		padding-top:35px;
	 	height: 60px;
	 	border-radius: 6px;
	 	border: 3px solid #2c3e50;
	 	background-color: #f1c40f;
	 	text-align: center;
	 }

	 tr {
	 	text-align:center;
	 	border: 1px solid #444;
	 }

	 #goback {
	 	width: 12%;
		margin: auto;
		height: 40px;
		background-color: #3498db;
		color: white;
		border-radius: 6px;
		text-align: center;
		padding-top: 12px;
		margin-top: 15px;
	 }
	 a {
	 	color: white;
	 	text-decoration: none;
	 }
	 #error-display {
	     padding-top: 120px;
	 }
</style>
</head>
<body>
	<div id="error-display">
	<h3>INVALID INPUTS PROVIDED :( </h3>
	<table>
      <c:forEach items="${requestScope.violations}" var="current">
        <tr>
          <td><c:out value="${current}" /><td>
        </tr>
      </c:forEach>
    </table>
	<div id="goback">Go Back <a href="addCustomer.html">Home Page</a></div>
	</div>
</body>
</html>