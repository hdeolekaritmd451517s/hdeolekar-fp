/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import edu.iit.sat.itmd4515.hdeolekar.model.CoBorrower;
import edu.iit.sat.itmd4515.hdeolekar.service.JPAWebEntityManager;

/**
 * This class provides persistence features for CoBorrower entity. CoBorrower is an entity associated
 * with loan application. It represents person who will be co-applicant in the loan application.
 * <b>Note:</b> This entity is managed by cascading feature in the loan application, hence, not used explicitly.
 * @author Harshal
 * @since Feb 13, 2017
 */
public class CoBorrowerDAO extends JPAWebEntityManager{
	@EJB
	JPAWebEntityManager jpaWebEntityManager;

	/**
	 * This method persists the coborrower in the coborrower table.
	 * @param coBorrower Coborrower object to be inserted.
	 */
	public void persistCoBorrower(CoBorrower coBorrower){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		em.persist(coBorrower);
	}

	/**
	 * This method deletes the coborrower record from the coborrower table.
	 * @param coBorrower coborrower record to be deleted
	 */
	public void removeCoBorrower(CoBorrower coBorrower){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		em.remove(coBorrower);
	}

	/**
	 * This method returns the list of coborrowers from the coborrower table.
	 * @return list of coborrowers
	 */
	public List<CoBorrower> getAllCoBorrower(){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return em.createQuery("select cb from CoBorrower cb", CoBorrower.class).getResultList();
	}

	/**
	 * This method returns coborrower record from the coborrower database table
	 * @param coborrowersId primary key of coborrower table
	 * @return coborrower record
	 */
	public  CoBorrower getCoBorrowerById(long coborrowersId){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return (CoBorrower) em.find(CoBorrower.class, coborrowersId);
	}

	/**
	 * This method returns the coborrower record based on loan application id.
	 * @param loanApplicationId loan application id associated with loan application.
	 * @return list of coborrowers
	 */
	public List<CoBorrower> getAllCoBorrowerByLoanApplicationId(long loanApplicationId){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		Query query = em.createQuery("select cb from CoBorrower cb where cb.loanAppId = :loanApplicationId", CoBorrower.class);
		query.setParameter("loanApplicationId", loanApplicationId);
		return (List<CoBorrower>)query.getResultList();
	}
}
