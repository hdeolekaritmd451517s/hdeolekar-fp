/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.model;

/**
 * This enum represents the type of the loan.
 * @author Harshal
 * @since Feb 9, 2017
 */
public enum LoanType {
	HOME_LOAN,
	CAR_LOAN,
	USED_CAR_LOAN,
	EDUCATION_LOAN
}