package edu.iit.sat.itmd4515.hdeolekar.model;

import java.util.Date;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import edu.iit.sat.itmd4515.hdeolekar.security.User;
import edu.iit.sat.itmd4515.hdeolekar.validator.SSN;

/**
 * This class represent customer entity for the loan application.
 * @author Harshal
 * @since Feb 5, 2017
 */
@Entity
@Table(name="customer")
@NamedQueries({
    @NamedQuery(name = "Customer.findAll", query = "select c from Customer c"),
    @NamedQuery(name = "Customer.findByUsername", query = "select c from Customer c where c.user.userName = :username")
})
@Named
@ViewScoped
public class Customer {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotNull(message = "You must enter a first name.")
	@Column(name="firstName")
	private String firstName;
	@NotNull(message = "You must enter a last name.")
	@Column(name="lastName")
	private String lastName;

	@NotNull(message = "You must valid a social security number.")
	@Column(name="ssn")
	@SSN
	private String socialSecurityNumber;
	@NotNull(message = "You select gender.")
	@Column(name="gender")
	@Enumerated(EnumType.STRING)
	private Gender gender;
	@NotNull(message = "You must enter date of birth.")
	@Temporal(TemporalType.DATE)
	@Column(name="dateOfBirth")
	private Date dateOfBirth;
	@NotNull(message = "You must enter valid phone number.")
	@Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid Phone Number")
	/* Phone Number formats: (xxx)xxx-xxxx; xxxxxxxxxx; xxx-xxx-xxxx
	 * 	1. ^\\(? : May start with an option "(" .
	 * 2. (\\d{3}): Followed by 3 digits.
	 * 3. \\)? : May have an optional ")"
	 * 4. [- ]? : May have an optional "-" after the first 3 digits or after optional ) character.
	 * 5. (\\d{3}) : Followed by 3 digits.
	 * 6. [- ]? : May have another optional "-" after numeric digits.
	 * 7. (\\d{4})$ : ends with four digits.
	 * Examples: Matches following phone numbers:
	 * (123)456-7890, 123-456-7890, 1234567890, (123)-456-7890
	 */
	@Column(name="phone")
	private String phone;
	@NotNull(message = "You must valid a email address.")
	@Column(name="email")
	private String email;
	@Valid
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="addressId")
	private Address address;
    @OneToOne
    @JoinColumn(name = "username")
    private User user;

	public Customer() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param firstName
	 * @param lastName
	 * @param socialSecurityNumber
	 * @param gender
	 * @param dateOfBirth
	 * @param phone
	 * @param email
	 * @param address
	 */
	public Customer(String firstName, String lastName, String socialSecurityNumber, Gender gender, Date dateOfBirth,
			String phone, String email, Address address) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.phone = phone;
		this.email = email;
		this.address = address;
	}



	/**
	 * @param firstName
	 * @param lastName
	 * @param socialSecurityNumber
	 * @param gender
	 * @param dateOfBirth
	 * @param phone
	 * @param email
	 * @param address
	 * @param user
	 */
	public Customer(String firstName, String lastName, String socialSecurityNumber, Gender gender, Date dateOfBirth,
			String phone, String email, Address address, User user) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.phone = phone;
		this.email = email;
		this.address = address;
		this.user = user;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

/*	*//**
	 * @return the loanApplication
	 *//*
	public Collection<LoanApplication> getLoanApplication() {
		return loanApplication;
	}

	*//**
	 * @param loanApplication the loanApplication to set
	 *//*
	public void setLoanApplication(Collection<LoanApplication> loanApplication) {
		this.loanApplication = loanApplication;
	}

	*//**
	 * @return the loan
	 *//*
	public Collection<LoanDetails> getLoan() {
		return loan;
	}

	*//**
	 * @param loan the loan to set
	 *//*
	public void setLoan(Collection<LoanDetails> loan) {
		this.loan = loan;
	}
*/
	/**
	 * @return the socialSecurityNumber
	 */
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	/**
	 * @param socialSecurityNumber the socialSecurityNumber to set
	 */
	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	/**
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Customer [id=");
		builder.append(id);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
/*		builder.append(", loanApplication=");
		builder.append(loanApplication);*/
		builder.append(", socialSecurityNumber=");
		builder.append(socialSecurityNumber);
		builder.append(", gender=");
		builder.append(gender);
		builder.append(", dateOfBirth=");
		builder.append(dateOfBirth);
		builder.append(", phone=");
		builder.append(phone);
		builder.append("]");
		return builder.toString();
	}
}
