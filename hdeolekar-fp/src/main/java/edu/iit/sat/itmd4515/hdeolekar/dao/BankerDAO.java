
package edu.iit.sat.itmd4515.hdeolekar.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import edu.iit.sat.itmd4515.hdeolekar.model.Banker;
import edu.iit.sat.itmd4515.hdeolekar.service.JPAWebEntityManager;

/**
 * This class is used to access data associated with bankers .
 * <b>Note:</b> This usage of this class in not been implemented in the project.
 * All the bankers are inserted from the insert.sql script.
 * @author Harshal
 * @since Apr 5, 2017
 */
@Stateless
public class BankerDAO {
	@EJB
	JPAWebEntityManager jpaWebEntityManager;

	public BankerDAO() {
		System.out.println("BankerDAO init called");
	}


	/**
	 * This method inserts new banker's record in banker table.
	 * @param banker to be inserted
	 */
	public void persistBanker(Banker banker){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		em.persist(banker);
	}

	/**
	 * This method removes or deletes the record from the banker table.
	 * @param banker to be removed
	 */
	public void removeBanker(Banker banker){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		em.remove(banker);
	}

	/**
	 * This method returns the list of bankers available from the banker table.
	 * @return list of all the bankers.
	 */
	public List<Banker> getAllBankers(){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		return em.createQuery("Banker.findAll", Banker.class).getResultList();
	}


	/**
	 * This method returns banker based on banker id
	 * @param bankerId primary key of banker table
	 * @return banker record
	 */
	public Banker getBankerById(long bankerId){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		return (Banker) em.find(Banker.class, bankerId);
	}

	/**
	 * This method returns banker record by username.
	 * @param username banker name
	 * @return banker record
	 */
	public Banker findByUsername(String username) {
		EntityManager em = jpaWebEntityManager.getEntityManager();
		if (em == null) {
			System.out.println("EntityManager is Null");
		}
		return em.createNamedQuery("Banker.findByUsername", Banker.class)
				.setParameter("username", username).getSingleResult();
	}
}
