/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.service;

import java.util.List;

import edu.iit.sat.itmd4515.hdeolekar.dao.CollateralDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.Collateral;

/**
 * This class provides services for adding, removing, searching collateral. Any interaction
 * with the persistence context will happens through this layer. Currently, managed by
 * cascading types.
 * @author Harshal
 * @since Feb 13, 2017
 */
public class CollateralService {
	private CollateralDAO collateralDAO;

	/**
	 * @param addressDAO
	 */
	public CollateralService(CollateralDAO collateralDAO) {
		super();
		this.collateralDAO = collateralDAO;
	}

	/**
	 * Registers collateral for the loan application
	 * @param collateral
	 */
	public void registerCollateral(Collateral collateral){
		collateralDAO.persistCollateral(collateral);
	}

	/**
	 * Deletes collateral for the loan application
	 * @param collateral collateral to be deleted.
	 */
	public void deleteCollateral(Collateral collateral) {
		collateralDAO.removeCollateral(collateral);
	}

	/**
	 * Returns list of all the collaterals
	 * @return list of collaterals.
	 */
	public List<Collateral> getAllCollateral(){
		return collateralDAO.getAllCollateral();
	}

	/**
	 * Returns collateral by id.
	 * @param collateralId primary key of collateral table.
	 * @return
	 */
	public Collateral getCollateralById(long collateralId){
		return collateralDAO.getCollateralById(collateralId);
	}
}
