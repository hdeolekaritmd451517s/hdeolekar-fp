/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.persistence.EntityManager;

import edu.iit.sat.itmd4515.hdeolekar.model.Collateral;
import edu.iit.sat.itmd4515.hdeolekar.service.JPAWebEntityManager;

/**
 *  This class provides persistence features for Collateral entity.
 *  Collateral entity is managed by cascading types, hence, not used explicitly.
 * @author Harshal
 * @since Feb 13, 2017
 */
public class CollateralDAO extends JPAWebEntityManager {
	@EJB
	JPAWebEntityManager jpaWebEntityManager;

	/**
	 * This method saves the collateral record in collateral database table.
	 * @param collateral collateral object to be persisted
	 */
	public void persistCollateral(Collateral collateral){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		em.persist(collateral);
	}

	/**
	 * This method removes the collateral record from the database.
	 * @param collateral collateral object to be removed.
	 */
	public void removeCollateral(Collateral collateral){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		em.remove(collateral);
	}

	/**
	 * This method returns list of all the collaterals.
	 * @return list of all the collaterals available.
	 */
	public List<Collateral> getAllCollateral(){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return em.createQuery("select c from Collateral c", Collateral.class).getResultList();
	}

	/**
	 * This method returns collateral by collateral id
	 * @param collateralId primary key of collateral table
	 * @return collateral object.
	 */
	public Collateral getCollateralById(long collateralId){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return (Collateral) em.find(Collateral.class, collateralId);
	}
}
