/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.model;

/**
 * This enum represents type of collateral provided by the customer.
 * @author Harshal
 * @since Feb 9, 2017
 */
public enum CollateralType {
	HOUSE,
	FLAT,
	FIXED_DEPOSIT,
	NON_AGRICULTURAL_LAND,
	OTHER
}