/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.validator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * This annoation represent annoation to be used for SSN.
 * @author Harshal
 * @since Feb 20, 2017
 */
@Constraint(validatedBy = SSNValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
public @interface SSN {
	  String message() default "Invalid Social Security Number";

	  Class<?>[] groups() default {};

	  Class<? extends Payload>[] payload() default {};
}
