package edu.iit.sat.itmd4515.hdeolekar.timer;

import javax.ejb.Stateless;

/**
 * This class represent timer task implementation.
 * @author Harshal
 * @since May 6, 2017
 */
@Stateless
public class HelloService {
    public String sayHello(){
        return "Hello from control: " + System.currentTimeMillis();
    }
}
