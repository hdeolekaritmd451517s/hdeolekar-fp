package edu.iit.sat.itmd4515.hdeolekar.policy;

import java.math.BigDecimal;
import java.util.Comparator;

import edu.iit.sat.itmd4515.hdeolekar.model.LoanApplication;

/**
 * This class implemented the Comparator interface, and overrides the compare()
 * method to compare LoanApplication objects based on gender, monthly income of customer,
 * loan amount, loan application date.
 *
 * @author Harshal Deolekar @ version 1.0 build 1.0
 * @since May 2, 2017
 */
public class LoanPolicyComparator implements Comparator<LoanApplication> {

	/**
	 * Method compare LoanApplication objects based on gender, monthly income of customer,
	 * loan amount, loan application date.
	 *
	 * @return sorted list of loan application based on applied policy.
	 */
	@Override
	public int compare(LoanApplication o1, LoanApplication o2) {
		String o1Gender, o2Gender;
		BigDecimal o1Income, o2Income, o1LoanAmount, o2LoanAmount;

		o1Gender = o1.getCustomer().getGender().toString();
		o2Gender = o2.getCustomer().getGender().toString();

		o1Income = o1.getMonthlyIncome();
		o2Income = o2.getMonthlyIncome();

		o1LoanAmount = o1.getLoanAmount();
		o2LoanAmount = o2.getLoanAmount();

		// order by highest income
		int incomeResult = -(o1Income.compareTo(o2Income));
		if (incomeResult != 0)
			return incomeResult;

		// then order by females
		int genderResult = o1Gender.compareTo(o2Gender);
		if (genderResult != 0)
			return genderResult;

		// then order by highest loan amount
		int loanAmountResult = -(o1LoanAmount.compareTo(o2LoanAmount));
		if (loanAmountResult != 0)
			return loanAmountResult;

		// finally order by loan application date.
		return (o1.getLoanApplicationDate().compareTo(o2.getLoanApplicationDate()));
	}
}