/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.model;

import java.math.BigDecimal;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * This class represents co-borrowers for the loan application.
 * @author Harshal
 * @since Feb 5, 2017
 */
@Entity
@ViewScoped
public class CoBorrower {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	@Column(name="firstName")
	private String firstName;
	@Column(name="lastName")
	private String lastName;
	@Column(name="relationship")
	private String relationship;
	@Column(name="netMonthlyIncome")
	private BigDecimal netMonthlyIncome;
	@Column(name="employmentType")
	@Enumerated(EnumType.STRING)
	private EmploymentType employmentType;
	@ManyToOne
	@JoinColumn(name="loanDetailsId")
	private LoanDetails loanDetails;
/*	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="loanAppId")
	private LoanApplication loanApp;*/

	public CoBorrower() {

	}

	public CoBorrower(String firstName, String lastName, String relationship, BigDecimal netMonthlyIncome,
			EmploymentType employmentType) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.relationship = relationship;
		this.netMonthlyIncome = netMonthlyIncome;
		this.employmentType = employmentType;
		//this.loanApp = loanApp;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	/**
	 * @return the relationshipWithApplicant
	 */
	public String getRelationship() {
		return relationship;
	}

	/**
	 * @param relationship the relationshipWithApplicant to set
	 */
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	/**
	 * @return the employmentType
	 */

	public EmploymentType getEmploymentType() {
		return employmentType;
	}

	/**
	 * @param employmentType the employmentType to set
	 */
	public void setEmploymentType(EmploymentType employmentType) {
		this.employmentType = employmentType;
	}

	/**
	 * @return the netMonthlyIncome
	 */
	public BigDecimal getNetMonthlyIncome() {
		return netMonthlyIncome;
	}

	/**
	 * @param netMonthlyIncome the netMonthlyIncome to set
	 */
	public void setNetMonthlyIncome(BigDecimal netMonthlyIncome) {
		this.netMonthlyIncome = netMonthlyIncome;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}



	/**
	 * @return the loanDetails
	 */
	public LoanDetails getLoanDetails() {
		return loanDetails;
	}


	/**
	 * @param loanDetails the loanDetails to set
	 */
	public void setLoanDetails(LoanDetails loanDetails) {
		this.loanDetails = loanDetails;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CoBorrower [id=");
		builder.append(id);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", relationship=");
		builder.append(relationship);
		builder.append(", netMonthlyIncome=");
		builder.append(netMonthlyIncome);
		builder.append(", employmentType=");
		builder.append(employmentType);
		builder.append("]");
		return builder.toString();
	}
}
