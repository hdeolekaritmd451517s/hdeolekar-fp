package edu.iit.sat.itmd4515.hdeolekar.web;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.service.CustomerService;
import edu.iit.sat.itmd4515.hdeolekar.service.LoanApplicationService;

/**
 * Servlet implementation class CustomerController. The purpose of this class is to service customer request.
 * @author Harshal
 * @since March 24, 2017
 */
@Named
@Stateless
@RequestScoped
public class CustomerController extends BaseController {
	private static final Logger LOG = Logger.getLogger(CustomerController.class.getName());
	@EJB
	private CustomerService customerService;
	@EJB
	private LoanApplicationService loanApplicationService;

	private Customer customer;

	@Override
	@PostConstruct
	public void init() {
		super.init();
		System.out.println("Customer Instantiated");
		this.customer = new Customer();
	}

	/**
	 * @return the customerService
	 */
	public CustomerService getCustomerService() {
		return customerService;
	}

	/**
	 * @param customerService the customerService to set
	 */
	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * Registers the customer.
	 * @return welcome page
	 */
	public String registerCustomer() {
		try {
			System.out.println("Registering customer");
		} catch (Exception e) {
			LOG.info(e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Registration Failed",
            		"Invalid Values"));
            return "/security/register.xhtml";
		}
		return "/welcome.xhtml";
	}

	/**
	 * This method updates the customer
	 * @return success or failure page.
	 */
	public String updateCustomer() {
		try {
			LOG.info("Updating Customer["+customer.getId()+"]");
			customerService.updateCustomer(customer);
		} catch (Exception e) {
			 LOG.log(Level.SEVERE, null, e);
			 context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Update Failed",
					 "Error Occured while updating customer."));
			 return "/banker/layout/updateProfile.xhtml";
		}
		return "/result/success/customerUpdateSuccess.xhtml";
	}

	/**
	 * This method deletes the customer.
	 * @return success or failure view.
	 */
	public String deleteCustomer() {
		try {
			LOG.info("Deleting Customer["+customer.getId()+"]");
			loanApplicationService.deleteLoanApplicationByCustomer(customer);
			customerService.deleteCustomer(customer);
		} catch (Exception e) {
			 LOG.log(Level.SEVERE, null, e);
			 context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Delete Failed",
					 "Error Occured while deleting customer."));
			 return "/result/fail/profileDeleteFailed.xhtml";
		}
		return "/result/success/profileDeleteSuccess.xhtml";
	}

	/**
	 * Returns list of all the customers.
	 * @return list of customer.
	 */
	public List<Customer> getAllCustomers() {
		System.out.println("get all customers");
		return customerService.getAllCustomers();
	}

	/**
	 * Returns customer by id
	 * @param customerId primary key of customer table.
	 */
	public void getCustomerById(long customerId) {
		customer = customerService.getCustomerById(customerId);
	}

	/**
	 * Returns customer by username
	 * @param username customer username.
	 */
	public void getCustomerByUsername(String username) {
		customer =  customerService.getCustomerByUsername(username);
	}
}
