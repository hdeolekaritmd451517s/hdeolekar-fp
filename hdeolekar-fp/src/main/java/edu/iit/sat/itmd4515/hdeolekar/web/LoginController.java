package edu.iit.sat.itmd4515.hdeolekar.web;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import edu.iit.sat.itmd4515.hdeolekar.service.CustomerService;


/**
 *
 * Servlet implementation class LoginController. This controller is used to
 * navigate based on the user logged in.
 * @author Prof. Scott
 * @author Harshal
 */
@Named
@Stateless
@RequestScoped
public class LoginController extends BaseController{
	private static final Logger LOG = Logger.getLogger(LoginController.class.getName());

	@NotNull(message = "You must enter a username.")
	private String username;
	@NotNull(message = "You must enter a password.")
	private String password;

	public LoginController() {
		super();
	}


	/**
	 * Returns the logged in username
	 * @return
	 */
	public String getRemoteUser() {
		this.context = FacesContext.getCurrentInstance();
		return context.getExternalContext().getRemoteUser();
	}

	/**
	 * Returns true if logged in user is customer
	 * @return true if logged in user is customer, else returns false
	 */
	public boolean isCustomer() {
		this.context = FacesContext.getCurrentInstance();
		return context.getExternalContext().isUserInRole("CUSTOMER_ROLE");
	}

	/**
	 * Returns true if logged in user is banker
	 * @return true if logged in user is banker, else returns false
	 */
	public boolean isBanker() {
		this.context = FacesContext.getCurrentInstance();
		return context.getExternalContext().isUserInRole("BANKER_ROLE");
	}

	/**
	 * Returns true if logged in user is admin
	 * @return true if logged in user is admin, else returns false
	 */
	public boolean isAdmin() {
		this.context = FacesContext.getCurrentInstance();
		return context.getExternalContext().isUserInRole("ADMIN_ROLE");
	}
	 // action methods
    /**
     * Handles login for the user
     * @return welcome page
     */
    public String doLogin() {
    	String redirectURL = "";
       LOG.info("Request received by LoginController..");
       System.out.println("Request received by LoginController..");
        try {
        	this.context = FacesContext.getCurrentInstance();
        	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
            request.login(username, password);
    		// If it is a customer page, show customers home page else show bankers
    		// home page.
    		if (isCustomer()) {
    			System.out.println("User Role is Customer");
    			//requestDispatcher = request.getRequestDispatcher("/customer");
    			redirectURL = "/customer/layout/welcome.xhtml";
    		} else if (isBanker() || isAdmin()) {
    			System.out.println("User Role is Banker or admin");
    			//requestDispatcher = request.getRequestDispatcher("/banker");
    			//redirectURL = "/banker/welcome.xhtml";
    			redirectURL = "/banker/layout/welcome.xhtml";
    		}
    		//requestDispatcher.forward(request, response);
        } catch (ServletException ex) {
            // login failed, first log the message
        	ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            // provide useful message to user - NOT the actual exception
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed Login",
            		"Username and/or Password was not valid."));
            // short circuit the navigation and send back to login page with message
            return "/security/login.xhtml";
        }

        return redirectURL+"?faces-redirect=true";
    }

    /**
     * Logs out the user
     * @return welcome page.
     */
    public String doLogout() {
        try {
        	this.context = FacesContext.getCurrentInstance();
            HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
            req.logout();
            System.out.println("logging out-- "+context.getExternalContext().getRequest());
        } catch (ServletException ex) {
            LOG.log(Level.SEVERE, null, ex);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed Logout", "Failed to logout."));
            return "/welcome.xhtml";
        }

        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "You have logged out", "You have logged out successfully.  Please don't forget to close your browser."));
        return "/welcome.xhtml";
    }

    /**
     * Get the value of username
     *
     * @return the value of username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the value of username
     *
     * @param username new value of username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the value of password
     *
     * @return the value of password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the value of password
     *
     * @param password new value of password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns registration view.
     * @return
     */
    public String showRegistrationPage() {
		System.out.println("register page called..");
		return "register.xhtml";
	}

    public String showAddressDetailsPage() {
		System.out.println("Address page called..");
		return null;
	}
}
