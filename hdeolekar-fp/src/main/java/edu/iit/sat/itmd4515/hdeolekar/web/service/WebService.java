package edu.iit.sat.itmd4515.hdeolekar.web.service;

import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.service.CustomerService;

@Path("/live")
public class WebService {
	private static final Logger LOG = Logger.getLogger(WebService.class.getName());

	@EJB
	CustomerService customerService;

	@POST
	@Path("/customer/{param}")
	public Response getCustomerById(@PathParam("param") String customerId) {
		System.out.println("WebService: getCustomerById["+customerId+"]");
		Customer customer = customerService.getCustomerById(Long.parseLong(customerId));
		return Response.status(200).entity(customer).build();
	}

}
