package edu.iit.sat.itmd4515.hdeolekar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import edu.iit.sat.itmd4515.hdeolekar.security.User;

/**
 * This class represent Banker entity.  Banker can approve or reject loan application.
 * @author Harshal
 * @since Feb 5, 2017
 */
@Entity
@Table(name="banker")
@NamedQueries({
    @NamedQuery(name = "Banker.findAll", query = "select b from Banker b"),
    @NamedQuery(name = "Banker.findByUsername", query = "select b from Banker b where b.user.userName = :username")
})
public class Banker {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull
	@Column(name="firstName")
	private String firstName;

	@NotNull
	@Column(name="lastName")
	private String lastName;

	@NotNull
	@Column(name="phone")
	private String phone;

	@NotNull
	@Column(name="email")
	private String email;

	@OneToOne
    @JoinColumn(name = "username")
    private User user;

	public Banker() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param firstName
	 * @param lastName
	 * @param phone
	 * @param email
	 * @param user
	 */
	public Banker(String firstName, String lastName, String phone, String email, User user) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.email = email;
		this.user = user;
	}


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}


}
