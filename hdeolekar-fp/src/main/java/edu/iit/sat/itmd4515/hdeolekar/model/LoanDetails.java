/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * This class represents the details of the loan after the loan has been sanctioned.
 * @author Harshal
 * @since Feb 4, 2017
 */
@Named
@Entity
@Table(name="loan_master")
@NamedQueries({
    @NamedQuery(name = "LoanDetails.findLoanDetailsByCustomerAndLoanApplicationId", query = "select l from LoanDetails l where l.customer.id = :customerId and l.loanApplication.id = :loanApplicationId")
})
public class LoanDetails {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotNull
	@Column(name="loanType")
	@Enumerated(EnumType.STRING)
	private LoanType loanType;
	@Column(name="applicationDate")
	@Temporal(TemporalType.DATE)
	private Date loanApplicationDate;
	@Column(name="sanctionDate")
	@Temporal(TemporalType.DATE)
	private Date loanSanctionDate;
	@Min(value=1)@Max(value=20)
	@Column(name="tenure")
	private int loanTenure;
	@Column(name="interestRate")
	private BigDecimal interestRate;
	@Column(name="loan_amount")
	private BigDecimal loanAmount;
	@Column(name="emi_amount")
	private BigDecimal emiAmount;
	@Column(name="emi_count")
	private int emiPaidCount;
	@Column(name="total_emi_count")
	private int totalEMICount;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="loanApplicationId")
	private LoanApplication loanApplication;
	@OneToOne(cascade=CascadeType.REMOVE)
	@JoinColumn(name="collateralType")
	private Collateral collateral;
	@OneToMany(cascade=CascadeType.REMOVE)
	@JoinTable(name="loan_coborrower" , joinColumns=@JoinColumn(name="loanDetailsId"), inverseJoinColumns=@JoinColumn(name="coborrowerId"))
	private Collection<CoBorrower> coborrowers = new ArrayList<CoBorrower>();
	@ManyToOne
	@JoinColumn(name="customerId")
	private Customer customer;
	@Transient
	private BigDecimal emiLateCharge;
/*	@OneToMany(cascade=CascadeType.ALL, mappedBy="loanDetails")
	@JoinTable(name="emi_payment" , joinColumns=@JoinColumn(name="loanDetailsId"), inverseJoinColumns=@JoinColumn(name="emiPaymentId"))
	private Collection<EMIPayment> emiPayment = new ArrayList<EMIPayment>();*/

	public LoanDetails() {
		// TODO Auto-generated constructor stub
	}


	public LoanDetails(LoanType loanType, Date loanApplicationDate, Date loanSanctionDate, int loanTenure, BigDecimal interestRate,
			BigDecimal loanAmount, Collateral collateral, Customer customer) {
		super();
		this.loanType = loanType;
		this.loanApplicationDate = loanApplicationDate;
		this.loanSanctionDate = loanSanctionDate;
		this.loanTenure = loanTenure;
		this.interestRate = interestRate;
		this.loanAmount = loanAmount;
		this.collateral = collateral;
		this.customer = customer;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the loanType
	 */
	public Enum getLoanType() {
		return loanType;
	}

	/**
	 * @param loanType the loanType to set
	 */
	public void setLoanType(LoanType loanType) {
		this.loanType = loanType;
	}


	/**
	 * @return the loanApplicationDate
	 */
	public Date getLoanApplicationDate() {
		return loanApplicationDate;
	}

	/**
	 * @param loanApplicationDate the loanApplicationDate to set
	 */
	public void setLoanApplicationDate(Date loanApplicationDate) {
		this.loanApplicationDate = loanApplicationDate;
	}

	/**
	 * @return the loanSanctionDate
	 */
	public Date getLoanSanctionDate() {
		return loanSanctionDate;
	}

	/**
	 * @param loanSanctionDate the loanSanctionDate to set
	 */
	public void setLoanSanctionDate(Date loanSanctionDate) {
		this.loanSanctionDate = loanSanctionDate;
	}


	/**
	 * @return the loanTenure
	 */
	public int getLoanTenure() {
		return loanTenure;
	}

	/**
	 * @param loanTenure the loanTenure to set
	 */
	public void setLoanTenure(int loanTenure) {
		this.loanTenure = loanTenure;
	}

	/**
	 * @return the interestRate
	 */
	public BigDecimal getInterestRate() {
		return interestRate;
	}

	/**
	 * @param interestRate the interestRate to set
	 */
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	/**
	 * @return the loanAmount
	 */
	public BigDecimal getLoanAmount() {
		return loanAmount;
	}

	/**
	 * @param loanAmount the loanAmount to set
	 */
	public void setLoanAmount(BigDecimal loanAmount) {
		this.loanAmount = loanAmount;
	}


	/**
	 * @return the collateral
	 */
	public Collateral getCollateral() {
		return collateral;
	}


	/**
	 * @param collateral the collateral to set
	 */
	public void setCollateral(Collateral collateral) {
		this.collateral = collateral;
	}


	/**
	 * @return the coborrowers
	 */
	public Collection<CoBorrower> getCoborrowers() {
		return coborrowers;
	}

	/**
	 * @param coborrowers the coborrowers to set
	 */
	public void setCoborrowers(Collection<CoBorrower> coborrowers) {
		this.coborrowers = coborrowers;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

/*	*//**
	 * @return the emiPayment
	 *//*
	public Collection<EMIPayment> getEmiPayment() {
		return emiPayment;
	}

	*//**
	 * @param emiPayment the emiPayment to set
	 *//*
	public void setEmiPayment(Collection<EMIPayment> emiPayment) {
		this.emiPayment = emiPayment;
	}
*/
	/**
	 * @return the loanApplication
	 */
	public LoanApplication getLoanApplication() {
		return loanApplication;
	}

	/**
	 * @param loanApplication the loanApplication to set
	 */
	public void setLoanApplication(LoanApplication loanApplication) {
		this.loanApplication = loanApplication;
	}

	/**
	 * @return the emiAmount
	 */
	public BigDecimal getEmiAmount() {
		return emiAmount;
	}

	/**
	 * @param emiAmount the emiAmount to set
	 */
	public void setEmiAmount(BigDecimal emiAmount) {
		this.emiAmount = emiAmount;
	}
	/**
	 * @return the emiPaidCount
	 */
	public int getEmiPaidCount() {
		return emiPaidCount;
	}

	/**
	 * @param emiPaidCount the emiPaidCount to set
	 */
	public void setEmiPaidCount(int emiPaidCount) {
		this.emiPaidCount = emiPaidCount;
	}

	/**
	 * @return the totalNoOfEMIt
	 */
	public int getTotalNoOfEMICount() {
		return totalEMICount;
	}

	/**
	 * @param totalEMICount the totalNoOfEMIt to set
	 */
	public void setTotalNoOfEMICount(int totalEMICount) {
		this.totalEMICount = totalEMICount;
	}


	/**
	 * @return the totalEMICount
	 */
	public int getTotalEMICount() {
		return totalEMICount;
	}

	/**
	 * @param totalEMICount the totalEMICount to set
	 */
	public void setTotalEMICount(int totalEMICount) {
		this.totalEMICount = totalEMICount;
	}

	/**
	 * @return the emiLateCharge
	 */
	public BigDecimal getEmiLateCharge() {
		return emiLateCharge;
	}

	/**
	 * @param emiLateCharge the emiLateCharge to set
	 */
	public void setEmiLateCharge(BigDecimal emiLateCharge) {
		this.emiLateCharge = emiLateCharge;
	}

	@Override
	public String toString() {
		return "LoanDetails [id=" + id + ", loanType=" + loanType + ", loanApplicationDate=" + loanApplicationDate
				+ ", loanSanctionDate=" + loanSanctionDate + ", loanTenure=" + loanTenure + ", interestRate="
				+ interestRate + ", loanAmount=" + loanAmount + ", emiAmount=" + emiAmount + ", emiPaidCount="
				+ emiPaidCount + ", totalEMICount=" + totalEMICount + ", collateral=" + collateral + ", coborrowers=" + coborrowers + ", customer=" + customer + "]";
	}
}
