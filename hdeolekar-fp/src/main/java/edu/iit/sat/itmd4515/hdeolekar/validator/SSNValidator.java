/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * This class validates SSN field.
 * @author Harshal
 * @since Feb 20, 2017
 */
public class SSNValidator implements ConstraintValidator<SSN, String>{

	/*	SSN format xxx-xx-xxxx, xxxxxxxxx, xxx-xxxxxx; xxxxx-xxxx:
	 * 1. ^\\d{3}: Starts with three numeric digits.
	 * 2. [- ]?: Followed by an optional "-"
	 * 3. \\d{2}: Two numeric digits after the optional "-"
	 * 4. [- ]?: May contain an optional second "-" character.
	 * 5. \\d{4}: ends with four numeric digits.
	 * Examples: 879-89-8989; 869878789 etc.
	 */
	private Pattern ssnPattern = Pattern.compile("^\\d{3}[- ]?\\d{2}[- ]?\\d{4}$");

	@Override
	public void initialize(SSN ssnAnnotation) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean result = true;
		if (value == null) {
			result = true;
		}

		Matcher m = ssnPattern.matcher(value);
		if (!m.matches()) {
			result = false;
		}
		return result;
	}
}
