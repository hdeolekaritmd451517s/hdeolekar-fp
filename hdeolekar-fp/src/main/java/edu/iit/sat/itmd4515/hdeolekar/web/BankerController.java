package edu.iit.sat.itmd4515.hdeolekar.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.iit.sat.itmd4515.hdeolekar.model.LoanApplication;
import edu.iit.sat.itmd4515.hdeolekar.service.LoanApplicationService;

/**
 * Servlet implementation class BankerController. This servlet controls banker related navigations.
 */
@WebServlet(description = "This servlet serves the banker requests", urlPatterns = { "/banker" })
public class BankerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private LoanApplicationService loanApplicationService;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BankerController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("User Role is Banker");
		  try (PrintWriter out = response.getWriter()) {
	            out.println("<!DOCTYPE html>");
	            out.println("<html>");
	            out.println("<head>");
	            out.println("<title>BankerController</title>");
	            out.println("</head>");
	            out.println("<body>");
	            out.println("<h1>Servlet BankerController at " + request.getContextPath() + "</h1>");
	            System.out.println(request.isUserInRole("BANKER_ROLE"));
	            out.println(request.isUserInRole("BANKER_ROLE"));
	            if (request.isUserInRole("BANKER_ROLE")) {
	                List<LoanApplication> l = loanApplicationService.getAllApprovedLoanApplication();
	                out.println("<h2>Logged in as: " + request.getUserPrincipal() + "</h2>");
	               // out.println("<h3>Logged in as: " + c.getUser().getGroups().forEach(g -> { g}); + "</h3>");
	                out.println("<ul>");

	                for (LoanApplication loanApplication : l) {
	                	out.println("<li> Date Applied: " + loanApplication.getLoanApplicationDate() + ", Loan Type: "+ loanApplication.getLoanType()
	                	+", Loan Type: "+ loanApplication.getCustomer().getFirstName()+" "+ loanApplication.getCustomer().getLastName()
	                	+", Customer Name: "+ loanApplication.getCustomer().getFirstName()+" "+loanApplication.getCustomer().getLastName()+"</li>");
					}

	                out.println("</ul>");
	            }

	            out.println("<a href=\"" + request.getContextPath() + "/logout\">Logout of the app</a>");

	            out.println("</body>");
	            out.println("</html>");
	        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
