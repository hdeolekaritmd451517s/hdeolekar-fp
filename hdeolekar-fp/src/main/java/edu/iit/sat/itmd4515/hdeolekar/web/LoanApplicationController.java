/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.web;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import edu.iit.sat.itmd4515.hdeolekar.email.SendHTMLEmail;
import edu.iit.sat.itmd4515.hdeolekar.model.CoBorrower;
import edu.iit.sat.itmd4515.hdeolekar.model.Collateral;
import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanApplication;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanDetails;
import edu.iit.sat.itmd4515.hdeolekar.service.CustomerService;
import edu.iit.sat.itmd4515.hdeolekar.service.LoanApplicationService;
import edu.iit.sat.itmd4515.hdeolekar.service.LoanDetailService;

/**
 * This class manages the loan application features.
 * @author Harshal
 * @since May 3, 2017
 */
@Named
@Stateless
@RequestScoped
public class LoanApplicationController extends BaseController {
	private static final Logger LOG = Logger.getLogger(LoanApplicationController.class.getName());
	private LoanApplication loanApplication;
	private Customer customer;
	private CoBorrower coBorrower1;
	private CoBorrower coBorrower2;
	private Collateral collateral;
	@EJB
	private CustomerService customerService;
	@EJB
	private LoanDetailService loanDetailService;
	@EJB
	private LoanApplicationService loanApplicationService;
	private List<LoanApplication> loanApplicationList;

	@PostConstruct
	@Override
    public void init() {
		super.init();
        this.loanApplication = new LoanApplication();
        this.coBorrower1 = new CoBorrower();
        this.coBorrower2 = new CoBorrower();
        this.customer = new Customer();
        this.collateral = new Collateral();
    }



	/**
	 * @return the loanApplication
	 */
	public LoanApplication getLoanApplication() {
		return loanApplication;
	}

	/**
	 * @param loanApplication the loanApplication to set
	 */
	public void setLoanApplication(LoanApplication loanApplication) {
		this.loanApplication = loanApplication;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the coBorrower1
	 */
	public CoBorrower getCoBorrower1() {
		return coBorrower1;
	}

	/**
	 * @param coBorrower1 the coBorrower1 to set
	 */
	public void setCoBorrower1(CoBorrower coBorrower1) {
		this.coBorrower1 = coBorrower1;
	}

	/**
	 * @return the coBorrower2
	 */
	public CoBorrower getCoBorrower2() {
		return coBorrower2;
	}

	/**
	 * @param coBorrower2 the coBorrower2 to set
	 */
	public void setCoBorrower2(CoBorrower coBorrower2) {
		this.coBorrower2 = coBorrower2;
	}

	/**
	 * @return the collateral
	 */
	public Collateral getCollateral() {
		return collateral;
	}

	/**
	 * @param collateral the collateral to set
	 */
	public void setCollateral(Collateral collateral) {
		this.collateral = collateral;
	}

	/**
	 * @return the loanApplicationList
	 */
	public List<LoanApplication> getLoanApplicationList() {
		return loanApplicationList;
	}

	/**
	 * @param loanApplicationList the loanApplicationList to set
	 */
	public void setLoanApplicationList(List<LoanApplication> loanApplicationList) {
		this.loanApplicationList = loanApplicationList;
	}

	/**
	 * Returns list of  all approved loan application
	 * @return list of  all approved loan application
	 */
	public List<LoanApplication> getAllApprovedLoanApplication() {
		return loanApplicationService.getAllApprovedLoanApplication();
	}

	/**
	 * Returns  list of  all rejected loan application
	 * @return  list of  all rejected loan application
	 */
	public List<LoanApplication> getAllRejectedLoanApplication() {
		return loanApplicationService.getAllRejectedLoanApplication();
	}

	/**
	 * Returns  list of  all new loan application
	 * @return  list of  all new loan application
	 */
	public List<LoanApplication> getAllPendingLoanApplication() {
		return loanApplicationService.getAllPendingLoanApplication();
	}

	/**
	 * Returns  list of  all cancelled loan application
	 * @return  list of  all cancelled loan application
	 */
	public List<LoanApplication> getAllCancelledLoanApplication() {
		return loanApplicationService.getAllCancelledLoanApplication();
	}

	/**
	 * Retrieves loan application based on id.
	 * @param loanId loan application id
	 * @return null or failure page
	 */
	public String getLoanApplicationById(long loanId) {
		FacesContext context = FacesContext.getCurrentInstance();
		System.out.println("Getting Loan Application");
		if (context.getExternalContext().isUserInRole("ADMIN_ROLE")
				|| context.getExternalContext().isUserInRole("BANKER_ROLE")) {
			System.out.println("Logged in: Admin or Banker");
			this.loanApplication = loanApplicationService.getLoanApplicationById(loanId);
		} else {
			System.out.println("Logged in: Customer");
			Customer loggedInCustomer = customerService
					.getCustomerByUsername(context.getExternalContext().getRemoteUser());
			System.out.println(loggedInCustomer);
			List<LoanApplication> loanApplicationList =  loanApplicationService.getLoanApplicationByCustomerAndLoanApplicationId(loggedInCustomer.getId(), loanId);
			if(loanApplicationList == null || loanApplicationList.isEmpty()) {
				System.out.println("loanApplicationList null");
				return "/result/fail/loanApplicationNotFound.xhtml";
			} else {
				System.out.println("loanApplicationList not null");
				this.loanApplication = loanApplicationList.get(0);
			}
		}
		return null;
	}

	/**
	 * Retrieves loan application based on customer.
	 * @param customerId customer id
	 * @return null or manage loan application page.
	 */
	public  String getLoanApplicationByCustomerId(long customerId) {
		try {
			loanApplicationList = loanApplicationService.getLoanApplicationByCustomerId(customerId);
		} catch (Exception e) {
			 LOG.log(Level.SEVERE, null, e);
			 context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
					 "Error Occured while fetching loan applications for customer"));
			 return "/banker/layout/manageLoanApplication.xhtml";
		}
		return null;
	}

	/**
	 * This method updates the loan application
	 * @return success or failure view
	 */
	public String updateLoanApplication() {
		try {
			this.loanApplication.setStatus("A");
			loanApplicationService.updateLoanApplication(loanApplication);
		} catch (Exception e) {
			 LOG.log(Level.SEVERE, null, e);
			 context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
					 "Error Occured while fetching loan applications for customer"));
			 return "/result/fail/loanApplicationUpdateFailed.xhtml";
		}

		return "/result/success/loanApplicationUpdateSuccess.xhtml";
	}

	/**
	 * This method approves the loan application
	 * @return success of failure view
	 */
	public String approveLoanApplication() {
		try {
			System.out.println("Approving loan application");
			// N - new, A - approved, R- rejected, C - cancelled
			loanApplication.setStatus("A");
			loanApplication.getLoanDetails().toString();
			loanApplication.getLoanDetails().setLoanSanctionDate(new Date());
			if(loanApplicationService == null) System.out.println("loanApplicationService is null");
			loanApplicationService.approveLoanApplication(loanApplication);
			System.out.println("Approving loan application");
			SendHTMLEmail sendEmail = new SendHTMLEmail();
			String message = sendEmail.getMessage(loanApplication);
			sendEmail.sendMessage(loanApplication.getEmail() ,message);
		} catch (Exception e) {
			 LOG.log(Level.SEVERE, null, e);
			 context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
					 "Error Occured while fetching loan applications for customer"));
			 return "/result/fail/loanApplicationUpdateFailed.xhtml";
		}
		return "/result/success/loanApplicationApproved.xhtml";
	}

	/**
	 * Rejects the loan application
	 * @return success or failure view.
	 */
	public String rejectLoanApplication() {
		try {
			// N - new, A - approved, R- rejected, C - cancelled
			System.out.println("Rejecting loan application");
			this.loanApplication.setStatus("R");
			System.out.println("Rejecting loan "+loanApplication);
			loanApplicationService.rejectLoanApplication(loanApplication);
			System.out.println("Rejecting loan application");
		} catch (Exception e) {
			 LOG.log(Level.SEVERE, null, e);
			 context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
					 "Error Occured while fetching loan applications for customer"));
			 return "/banker/layout/manageLoanApplication.xhtml";
		}
		return "/result/fail/loanApplicationReject.xhtml";
	}


	/**
	 * Submits or creates the loan application for the customer
	 * @return success or failure view.
	 */
	public String submitLoanApplication(){
		LOG.info(loanApplication.toString());
		LOG.info(collateral.toString());
		LOG.info(coBorrower1.toString());
		LOG.info(coBorrower2.toString());
		LOG.info(customer.toString());

		try {
			loanApplication.setCustomer(customerService.getCustomerById(customer.getId()));
			loanApplication.getCoborrower().add(coBorrower1);
			loanApplication.getCoborrower().add(coBorrower2);
			loanApplication.setCollateral(collateral);
			loanApplication.setLoanApplicationDate(new Date());
			loanApplication.setStatus("N");

			LoanDetails loanDetails = new LoanDetails();
			loanDetails.setLoanAmount(loanApplication.getLoanAmount());
			loanDetails.setLoanApplicationDate(loanApplication.getLoanApplicationDate());
			loanDetails.setInterestRate(new BigDecimal(12));
			loanDetails.setLoanTenure(10);
			loanDetails.setLoanType(loanApplication.getLoanType());
			loanDetails.setCustomer(loanApplication.getCustomer());
			loanDetails.getCoborrowers().addAll(loanApplication.getCoborrower());
			loanDetails.setCollateral(loanApplication.getCollateral());
			loanDetails.setLoanApplication(loanApplication);
			loanDetails.setEmiAmount(loanApplicationService.calculateLoanEMIPayment(loanDetails));
			loanDetails.setEmiPaidCount(0);
			loanDetails.setTotalNoOfEMICount(loanApplicationService.calculateTotalNumberOfInstallment(loanDetails));

			loanApplication.setLoanDetails(loanDetails);
			coBorrower1.setLoanDetails(loanDetails);
			coBorrower2.setLoanDetails(loanDetails);

			loanApplicationService.submitLoanApplication(this.loanApplication);
		} catch (Exception e) {
			 LOG.log(Level.SEVERE, null, e);
			 context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
					 "Error Occured while creating loan applications for customer"));
			 return "/result/fail/loanApplicationCreateFailed.xhtml";
		}
		return "/result/success/loanApplicationCreateSuccess.xhtml";
	}

	/**
	 * This method deletes the loan application
	 * @return success or failure view
	 */
	public String deleteLoanApplication(){
		try {
			System.out.println("Deleting loan application");
			// N - new, A - approved, R- rejected, C - cancelled
			System.out.println("Loan Application: "+loanApplication);
			System.out.println("deleting loan application");
			loanApplicationService.deleteLoanApplication(loanApplication);
		} catch (Exception e) {
			 LOG.log(Level.SEVERE, null, e);
			 context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
					 "Error Occured while cancelling loan applications for customer"));
			 return "/result/fail/loanApplicationDeleteFailed.xhtml";
		}
		return "/result/success/loanApplicationDeleteSuccess.xhtml";
	}

	/**
	 * This method cancels the loan application
	 * @return success or failure view.
	 */
	public String cancelLoanApplication(){
		try {
			System.out.println("Cancelling loan application");
			// N - new, A - approved, R- rejected, C - cancelled
			System.out.println("Loan Application: "+loanApplication);
			if(this.loanApplication.getStatus().equalsIgnoreCase("A") || loanApplication.getStatus().equalsIgnoreCase("C")  ||loanApplication.getStatus().equalsIgnoreCase("R") ) {
				System.out.println("Cannot cancel loan application");
				return "/result/fail/loanApplicationCancelFailed.xhtml";
			} else {
				System.out.println("Cancelling loan application with status new");
				this.loanApplication.setStatus("C");
				loanApplicationService.cancelLoanApplication(loanApplication);
			}
		} catch (Exception e) {
			 LOG.log(Level.SEVERE, null, e);
			 context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
					 "Error Occured while cancelling loan applications for customer"));
			 return "/result/fail/loanApplicationCancelFailed.xhtml";
		}
		return "/result/success/loanApplicationCancelSuccess.xhtml";
	}

}
