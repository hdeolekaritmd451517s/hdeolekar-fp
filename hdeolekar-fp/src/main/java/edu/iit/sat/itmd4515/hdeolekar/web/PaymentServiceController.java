/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.web;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.model.EMIPayment;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanDetails;
import edu.iit.sat.itmd4515.hdeolekar.service.CustomerService;
import edu.iit.sat.itmd4515.hdeolekar.service.LoanDetailService;
import edu.iit.sat.itmd4515.hdeolekar.service.PaymentService;

/**
 * This class manages the information required on payment page.
 * @author Harshal
 * @since May 3, 2017
 */
@Named
@Stateless
@RequestScoped
public class PaymentServiceController extends BaseController {
	private static final Logger LOG = Logger.getLogger(PaymentServiceController.class.getName());
	@EJB
	private PaymentService paymentService ;
	@EJB
	private CustomerService customerService ;
	@EJB
	private LoanDetailService loanDetailService;

	private List<EMIPayment> emiPaymentsList;

	public PaymentServiceController() {

	}


	/**
	 * This method submits the emi payment
	 * @param loanDetails loan details or emi to be paid
	 * @return success or failure view.
	 */
	public String submitEMIPayment(LoanDetails loanDetails){
		try {
			EMIPayment emiPayment = new EMIPayment();
			emiPayment.setCustomer(loanDetails.getCustomer());
			emiPayment.setEmiAmount(loanDetails.getEmiAmount());
			emiPayment.setEmiDateTime(new Date());
			emiPayment.setInstallmentNo(loanDetails.getEmiPaidCount() + 1);
			emiPayment.setLateFineCharge(loanDetails.getEmiLateCharge());
			emiPayment.setLoanDetails(loanDetails);
			emiPayment.setTotalAmount(new BigDecimal(loanDetails.getEmiAmount().intValue()+ loanDetails.getEmiLateCharge().intValue()));
			paymentService.submitEMIPayment(emiPayment);
		} catch (Exception e) {
			 LOG.log(Level.SEVERE, null, e);
			 context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Payment Failed",
					 "Error Occured while transaction."));
			 return "/result/failed/paymentFailure.xhtml";
		}
		return "/result/success/paymentSuccess.xhtml";
	}

	/**
	 * Retrievew list of all the emi payments.
	 * @return null
	 */
	public String getAllEMIPayments(){
		emiPaymentsList = paymentService.getAllEMIPayments();
		return  null;
	}


	/**
	 * Retrieves list of emi paid by customer
	 * @param loanApplicationId loan application id
	 * @return null
	 */
	public String getAllEMIPaymentsByLoanApplicationId(long loanApplicationId) {
		this.context = FacesContext.getCurrentInstance();
		if(context.getExternalContext().isUserInRole("CUSTOMER_ROLE")) {
			Customer loggedInCustomer = customerService.getCustomerByUsername(context.getExternalContext().getRemoteUser());
			System.out.println(loggedInCustomer);
			emiPaymentsList = paymentService.getAllEMIPaymentsByCustomerId(loggedInCustomer.getId(), loanApplicationId);
			if(emiPaymentsList == null)
				System.out.println("List is null");
			else
				System.out.println(emiPaymentsList);
		} else {
			emiPaymentsList = paymentService.getAllEMIPaymentsByLoanApplicationId(loanApplicationId);
		}
		return null;
	}

	/**
	 * Returns list of emi paid
	 * @return the emiPaymentsList
	 */
	public List<EMIPayment> getEmiPaymentsList() {
		return emiPaymentsList;
	}

	/**
	 * Sets list of emi paid.
	 * @param emiPaymentsList the emiPaymentsList to set
	 */
	public void setEmiPaymentsList(List<EMIPayment> emiPaymentsList) {
		this.emiPaymentsList = emiPaymentsList;
	}
}
