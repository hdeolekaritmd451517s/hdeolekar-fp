package edu.iit.sat.itmd4515.hdeolekar.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * This class represents loan application.
 * @author Harshal
 * @since Apr 23, 2017
 */
@Named
@Entity
@NamedQueries({
    @NamedQuery(name = "Customer.findAll", query = "select c from Customer c"),
    @NamedQuery(name = "LoanApplication.findAllLoanApplicationByCustomer", query = "select l from LoanApplication l where l.customer.id=:customerId"),
    @NamedQuery(name = "LoanApplication.findAllApprovedLoanApplication", query = "select l from LoanApplication l where l.status='A'"),
    @NamedQuery(name = "LoanApplication.findAllRejectedLoanApplication", query = "select l from LoanApplication l where l.status='R'"),
    @NamedQuery(name = "LoanApplication.findAllPendingLoanApplication", query = "select l from LoanApplication l where l.status='N'"),
    @NamedQuery(name = "LoanApplication.findAllCancelledLoanApplication", query = "select l from LoanApplication l where l.status='C'"),
    @NamedQuery(name = "LoanApplication.deleteLoanApplicationByCustomer", query = "delete from LoanApplication l where l.customer.id=:customerId")
})
@Table (name = "loan_application")
public class LoanApplication {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotNull
	@Column(name="loanType")
	@Enumerated(EnumType.STRING)
	private LoanType loanType;
	private BigDecimal loanAmount;
	private BigDecimal monthlyIncome;
	@NotNull
	@Column(name="phone")
	private String phone;
	@ManyToOne
	@JoinColumn(name="customerId")
	private Customer customer;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="loanDetailsId")
	private LoanDetails loanDetails;
	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable(name="loan_app_coborrower" , joinColumns=@JoinColumn(name="loapAppId"), inverseJoinColumns=@JoinColumn(name="coborrowerId"))
	private Collection<CoBorrower> coborrower = new ArrayList<CoBorrower>();
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="collateralID")
	private Collateral collateral;
	private String email;
	@Temporal(TemporalType.DATE)
	private Date loanApplicationDate;
	@Column(name="status")
	private String status;

	/**
	 * @param loanType
	 * @param loanAmount
	 * @param monthlyIncome
	 * @param phone
	 * @param customer
	 * @param coborrower
	 * @param email
	 */
	public LoanApplication(LoanType loanType, BigDecimal loanAmount, BigDecimal monthlyIncome, String email, String phone,
			Customer customer, Collection<CoBorrower> coborrower,  Collateral collateral) {
		super();
		this.loanType = loanType;
		this.loanAmount = loanAmount;
		this.monthlyIncome = monthlyIncome;
		this.phone = phone;
		this.customer = customer;
		this.coborrower = coborrower;
		this.collateral = collateral;
		this.email = email;
	}
	public LoanApplication() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the loanType
	 */
	public LoanType getLoanType() {
		return loanType;
	}
	/**
	 * @param loanType the loanType to set
	 */
	public void setLoanType(LoanType loanType) {
		this.loanType = loanType;
	}
	/**
	 * @return the loanAmount
	 */
	public BigDecimal getLoanAmount() {
		return loanAmount;
	}
	/**
	 * @param loanAmount the loanAmount to set
	 */
	public void setLoanAmount(BigDecimal loanAmount) {
		this.loanAmount = loanAmount;
	}

	/**
	 * @return the monthlyIncome
	 */
	public BigDecimal getMonthlyIncome() {
		return monthlyIncome;
	}
	/**
	 * @param monthlyIncome the monthlyIncome to set
	 */
	public void setMonthlyIncome(BigDecimal monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the emailID
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the loanApplicationDate
	 */
	public Date getLoanApplicationDate() {
		return loanApplicationDate;
	}
	/**
	 * @param loanApplicationDate the loanApplicationDate to set
	 */
	public void setLoanApplicationDate(Date loanApplicationDate) {
		this.loanApplicationDate = loanApplicationDate;
	}
	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}
	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	/**
	 * @return the coborrower
	 */
	public Collection<CoBorrower> getCoborrower() {
		return coborrower;
	}
	/**
	 * @param coborrower the coborrowerId to set
	 */
	public void setCoborrower(Collection<CoBorrower> coborrower) {
		this.coborrower = coborrower;
	}
	/**
	 * @return the collateral
	 */
	public Collateral getCollateral() {
		return collateral;
	}
	/**
	 * @param collateral the collateral to set
	 */
	public void setCollateral(Collateral collateral) {
		this.collateral = collateral;
	}
	/**
	 * @return the loanDetails
	 */
	public LoanDetails getLoanDetails() {
		return loanDetails;
	}
	/**
	 * @param loanDetails the loanDetails to set
	 */
	public void setLoanDetails(LoanDetails loanDetails) {
		this.loanDetails = loanDetails;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LoanApplication [id=" + id + ", loanType=" + loanType + ", loanAmount=" + loanAmount
				+ ", monthlyIncome=" + monthlyIncome + ", phone=" + phone + ", customer=" + customer + ", "
						+ " coborrower=" + coborrower + ", collateral=" + collateral + ", email=" + email
				+ ", loanApplicationDate=" + loanApplicationDate + ", status=" + status + "]";
	}
}
