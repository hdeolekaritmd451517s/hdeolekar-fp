/**
 * This package is for maintaining all the custom converter classes.
 */
package edu.iit.sat.itmd4515.hdeolekar.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * This class custom converts the status of application from indicative to more descriptive.
 * E.g If loan application has status as 'A' which indicates that the application is approved,
 * then, while rendering the status gets converted to "APPROVED"
 * @author Harshal
 * @since May 5, 2017
 */
@FacesConverter("ApplicationStatusConverter")
public class ApplicationStatusConverter implements Converter{

	/* *
	 *  This is default method implementation of Converter interface, it is used with <h:inputText>  fields.
	 *  Currently, there is no relevant usage.
	 */
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Object result = "";
		return result;
	}

	/* *
	 *  This method provides implementation of Converter interface abstract method, it is used with <h:outputText>  fields.
	 *  Currently, it is used to convert the status of loan application to more descriptive manner.
	 */
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		String status = value.toString();
		// if loan status is approved (A), new (N), reject (R), cancelled (C)
		if(status.equalsIgnoreCase("A"))
			status = "APPROVED";
		else if(status.equalsIgnoreCase("N"))
		status = "NEW";
		else if(status.equalsIgnoreCase("R"))
			status = "REJECT";
		else if(status.equalsIgnoreCase("C"))
			status = "CANCELLED";
		return status.toString();
	}
}