package edu.iit.sat.itmd4515.hdeolekar.email;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Loads configuration required for email.
 * @author Harshal
 * @since May 6, 2017
 */
public class Configuration {
	private static Properties properties = new Properties();
	public static final String MAIL_SMTP_HOST = "mail.smtp.host";
	public static final String MAIL_SMTP_SOCKETFACTORY_PORT = "mail.smtp.socketFactory.port";
	public static final String MAIL_SMTP_SOCKETFACTORY_CLASS = "mail.smtp.socketFactory.class";
	public static final String MAIL_SMTP_STARTTLS_FLAG = "mail.smtp.starttls.enable";
	public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	public static final String MAIL_SMTP_USER = "mailServerUserId";
	public static final String MAIL_SMTP_USER_PASS = "mailServerPassword";
	public static final String MAIL_SET_FROM = "setFromEmail";
	public static final String MAIL_SUBJECT_LINE = "mailSubjectLine";


	static {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		String resourceName = "config.properties"; // could also be a constant
		InputStream resourceStream = loader.getResourceAsStream(resourceName);
		if(resourceStream!=null){
			try {
				properties.load(resourceStream);
			    System.out.println("Message properties Loaded Successfully");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("stream is null");
		}
	}

	/**
	 * This method returns email configuration value based on key provided.
	 * @param key email settings key.
	 * @return email settings value.
	 */
	public static String get(String key) {
		return properties.getProperty(key) == null ? "NA" : properties.getProperty(key);
	}
}