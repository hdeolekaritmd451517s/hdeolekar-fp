/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.service;

import java.util.List;

import edu.iit.sat.itmd4515.hdeolekar.dao.AddressDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.Address;

/**
 * The class represents the service layer for Address class. Any interaction
 * with the persistence context will happen through this layer.
 *
 * @author Harshal
 * @since Feb 13, 2017
 */
public class AddressService {
	private AddressDAO addressDAO;

	/**
	 * @param addressDAO
	 */
	public AddressService(AddressDAO addressDAO) {
		super();
		this.addressDAO = addressDAO;
	}

	public void registerAddress(Address address) {
		addressDAO.persistAddress(address);
	}

	public void deleteAddress(Address address) {
		addressDAO.removeAddress(address);
	}

	public List<Address> getAllAddress(){
		return addressDAO.getAllAddress();
	}

	public Address getAddressById(long addressId){
		return addressDAO.getAddressById(addressId);
	}
}
