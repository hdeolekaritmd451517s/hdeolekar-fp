/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import edu.iit.sat.itmd4515.hdeolekar.model.Address;
import edu.iit.sat.itmd4515.hdeolekar.service.JPAWebEntityManager;

/**
 * This class provides persistence features for Address entity. Address is represented by address line1,
 * address line 2, city, state, country, pin code.
 * @author Harshal
 * @since Feb 13, 2017
 */
@Stateless
public class AddressDAO extends JPAWebEntityManager{

	@EJB
	JPAWebEntityManager jpaWebEntityManager;

	public AddressDAO() {

	}


	/**
	 * This method inserts the customer address in the address table.
	 * @param address Customer's address to be inserted
	 */
	public void persistAddress(Address address){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		em.persist(address);
	}


	/**
	 * This method deletes the customer address from the address table.
	 * @param address Customer's address to be deleted
	 */
	public void removeAddress(Address address){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		em.remove(address);
	}

	/**
	 * This method returns all addresses from address table
	 * @return list of customer address
	 */
	public List<Address> getAllAddress(){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return em.createQuery("select a from Address a", Address.class).getResultList();
	}

	/**
	 * This method returns address by address id
	 * @param addressId Customer's address id where id is the primary key in address table.
	 * @return Customer's address
	 */
	public Address getAddressById(long addressId){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return (Address) em.find(Address.class, addressId);
	}
}
