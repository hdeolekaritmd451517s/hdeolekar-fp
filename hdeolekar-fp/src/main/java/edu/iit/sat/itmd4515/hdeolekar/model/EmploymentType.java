/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.model;

/**
 * The enum represents the type of the employment a customer or a co-borrower may have.
 * @author Harshal
 * @since Feb 12, 2017
 */
public enum EmploymentType {
	SALARIED,
	SELF_EMPLOYED_BUSINESS,
	SELF_EMPLOYED_PROFESSIONAL,
	RETIRED,
	HOME_MAKER
}
