/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import edu.iit.sat.itmd4515.hdeolekar.dao.CustomerDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.Customer;

/**
 * This class provides services for Customer entity. E.g registering a customer
 *
 * @author Harshal
 * @since Feb 12, 2017
 */
@Named
@Stateless
@RequestScoped
public class CustomerService {
	private static final Logger LOG = Logger.getLogger(CustomerService.class.getName());
	@EJB
	private CustomerDAO customerServiceDAO;

	public CustomerService() {
		System.out.println("Customer Service Instantiated");
	}

	/**
	 * Registers the customer. Currently has some issue which is to be resolved.
	 * @return success page.
	 */
	public String registerCustomer() {
		try {
			System.out.println("Registering customer");
		} catch (Exception e) {
			LOG.info(e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Registration Failed",
            		"Invalid Values"));
            return "/security/register.xhtml";
		}
		return null;
	}

	/**
	 * Updates customer details.
	 * @param customer customer to be updated.
	 */
	public void updateCustomer(Customer customer) {
		customerServiceDAO.updateCustomer(customer);
	}

	/**
	 * Deletes the customer record.
	 * @param customer customer to be deleted.
	 */
	public void deleteCustomer(Customer customer) {
		customerServiceDAO.removeCustomer(customer);
	}

	/**
	 * Returns list of all the customers
	 * @return list of customers
	 */
	public List<Customer> getAllCustomers() {
		return customerServiceDAO.getAllCustomers();
	}

	/**
	 * Returns customer based on customer id.
	 * @param customerId primary key of customer table.
	 * @return customer object.
	 */
	public Customer getCustomerById(long customerId) {
		return customerServiceDAO.getCustomerById(customerId);
	}

	/**
	 * Returns customer based on customer username.
	 * @param username customer username.
	 * @return customer object.
	 */
	public Customer getCustomerByUsername(String username) {
		return customerServiceDAO.findByUsername(username);
	}

	/**
	 * @return the customerServiceDAO
	 */
	public CustomerDAO getCustomerServiceDAO() {
		return customerServiceDAO;
	}

	/**
	 * @param customerServiceDAO
	 *            the customerServiceDAO to set
	 */
	public void setCustomerServiceDAO(CustomerDAO customerServiceDAO) {
		this.customerServiceDAO = customerServiceDAO;
	}

}
