package edu.iit.sat.itmd4515.hdeolekar.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import edu.iit.sat.itmd4515.hdeolekar.dao.LoanApplicationDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanApplication;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanDetails;
import edu.iit.sat.itmd4515.hdeolekar.policy.LoanPolicyComparator;

/**
 * This class represents a service layer for the loan application.
 * @author Harshal
 * @since Feb 13, 2017
 */
@Named
@Stateless
@RequestScoped
public class LoanApplicationService{
	private static final Logger LOG = Logger.getLogger(LoanApplicationService.class.getName());
	@EJB
	private LoanApplicationDAO loanApplicationDAO;
	@EJB
	private CustomerService customerService;

	public LoanApplicationService() {
	}

	/**
	 * Returns list of approved loan application.
	 * @return
	 */
	public List<LoanApplication> getAllApprovedLoanApplication() {
		return loanApplicationDAO.getAllApprovedLoanApplication();
	}

	/**
	 * Returns list of all the approved loan by loan application policy
	 * @return list of approved loans
	 */
	public List<LoanApplication> getAllApprovedLoanApplicationByLoanApplicationPolicy() {
		List<LoanApplication> loanApplicationList = loanApplicationDAO.getAllApprovedLoanApplication();
		loanApplicationList.sort(new LoanPolicyComparator());
		System.out.println("Policy List=====================");
		System.out.println(loanApplicationList);
		return loanApplicationList;
	}


	/**
	 * Returns list of rejected loans
	 * @return
	 */
	public List<LoanApplication> getAllRejectedLoanApplication() {
		return loanApplicationDAO.getAllRejectedLoanApplication();
	}

	/**
	 * Return list of new loans
	 * @return
	 */
	public List<LoanApplication> getAllPendingLoanApplication() {
		return loanApplicationDAO.getAllPendingLoanApplication();
	}

	/**
	 * Returns list of cancelled loan application.
	 * @return
	 */
	public List<LoanApplication> getAllCancelledLoanApplication() {
		return loanApplicationDAO.getAllCancelledLoanApplication();
	}

	/**
	 * Return loan application by id
	 * @param loanId primary key of loan application table.
	 * @return loan application
	 */
	public LoanApplication getLoanApplicationById(long loanId) {
		return loanApplicationDAO.getLoanApplicationById(loanId);
	}

	/**
	 * Return list of loan application by customer id.
	 * @param customerId customer id
	 * @return list of loan applications
	 */
	public  List<LoanApplication> getLoanApplicationByCustomerId(long customerId) {
		return loanApplicationDAO.getLoanApplicationByCustomerId(customerId);
	}

	/**
	 * Returns list of loan applications based on customer and loan application id.
	 * @param customerId customer id
	 * @param loanApplicationId loan application id
	 * @return
	 */
	public  List<LoanApplication> getLoanApplicationByCustomerAndLoanApplicationId(long customerId, long loanApplicationId) {
		return loanApplicationDAO.getLoanApplicationByCustomerAndLoanApplicationId(customerId, loanApplicationId);
	}

	/**
	 * Returns all loan application based on customer
	 * @return list of loan applications.
	 */
	public  List<LoanApplication> getAllLoanApplicationByCustomer() {
		FacesContext context = FacesContext.getCurrentInstance();
		System.out.println("Getting Loan Application");
		if(context.getExternalContext().isUserInRole("ADMIN_ROLE") || context.getExternalContext().isUserInRole("BANKER_ROLE")) {
			System.out.println("Logged in: Admin or Banker");
			return loanApplicationDAO.getAllLoanApplication();
		} else {
			System.out.println("Logged in: Customer");
			Customer loggedInCustomer = customerService.getCustomerByUsername(context.getExternalContext().getRemoteUser());
			System.out.println(loggedInCustomer);
			return loanApplicationDAO.getLoanApplicationByCustomerId(loggedInCustomer.getId());
		}
	}

	/**
	 * This method updates the loan application
	 * @param loanApplication loan application to be updated
	 */
	public void updateLoanApplication(LoanApplication loanApplication) {
		loanApplication.setStatus("A");
		loanApplicationDAO.updateLoanApplication(loanApplication);
	}

	/**
	 * This method approves the loan application
	 * @param loanApplication  loan application to be approved.
	 */
	public void approveLoanApplication(LoanApplication loanApplication) {
		loanApplicationDAO.updateLoanApplication(loanApplication);
	}

	/**
	 * This method rejects the loan application
	 * @param loanApplication  loan application to be rejected.
	 */
	public void rejectLoanApplication(LoanApplication loanApplication) {
		// N - new, A - approved, R- rejected, C - cancelled
		loanApplication.setStatus("R");
		loanApplicationDAO.updateLoanApplication(loanApplication);
	}

	/**
	 * This method cancels the loan application for the customer.
	 * @param loanApplication loan application to be cancelled.
	 */
	public void cancelLoanApplication(LoanApplication loanApplication) {
		loanApplicationDAO.updateLoanApplication(loanApplication);
	}

	/**
	 * This method creates the loan application
	 * @param loanApplication loan application to be created.
	 */
	public void submitLoanApplication(LoanApplication loanApplication){
			loanApplicationDAO.persistLoanApplication(loanApplication);
	}

	/**
	 * This method deletes the loan application
	 * @param loanApplication loan application to be deleted.
	 */
	public void deleteLoanApplication(LoanApplication loanApplication){
		loanApplicationDAO.deleteLoanApplication(loanApplication);
	}

	/**
	 * This method deletes the loan application by customer id
	 * @param customer customer for which loan application is to be deleted.
	 */
	public void deleteLoanApplicationByCustomer(Customer customer){
		loanApplicationDAO.deleteLoanApplicationByCustomer(customer);
	}

	/**
	 * This method calculates the emi for the loan application based on provided loan details.
	 * @param loanDetails
	 * @return emi amount
	 */
	public BigDecimal calculateLoanEMIPayment(LoanDetails loanDetails) {
		// annual interest rate
		float interestRate = (loanDetails.getInterestRate().floatValue() / 100);
		// monthly interest rate
		double monthlyRate = interestRate / 12.0;
		// no of months/payments the EMI has to be paid for total loan
		int termInMonths = calculateTotalNumberOfInstallment(loanDetails);
		// monthly installments to be paid.
		double monthlyPayment = (loanDetails.getLoanAmount().intValue() * monthlyRate) / (1 - Math.pow(1 + monthlyRate, -termInMonths));
		return new BigDecimal(monthlyPayment).setScale(2, RoundingMode.CEILING);
	}

	/**
	 * This method returns total number installments.
	 * @param loanDetails loan details
	 * @return number of installment.
	 */
	public int calculateTotalNumberOfInstallment(LoanDetails loanDetails) {
		return loanDetails.getLoanTenure() * 12;
	}
}