/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.model;

/**
 * This enum represent the gender of a person.
 * @author Harshal
 * @since Feb 12, 2017
 */
public enum Gender {
	MALE, FEMALE
}
