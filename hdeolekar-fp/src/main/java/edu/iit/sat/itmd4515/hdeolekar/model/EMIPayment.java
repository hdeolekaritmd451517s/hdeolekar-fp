/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represents emi paid by the customer.
 * @author Harshal
 * @since Feb 8, 2017
 */
@Named
@Entity
@Table (name="emi_payment")
@NamedQueries({
    @NamedQuery(name = "EMIPayment.findAll", query = "select e from EMIPayment e"),
    @NamedQuery(name = "EMIPayment.findByCustomerId", query = "select e from EMIPayment e where e.customer.id = :customerId and e.loanDetails.loanApplication.id = :loanApplicationId"),
    @NamedQuery(name = "EMIPayment.findByLoanApplicationId", query = "select e from EMIPayment e where e.loanDetails.loanApplication.id = :loanApplicationId")
})
public class EMIPayment {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@ManyToOne
	@JoinColumn(name="customerId")
	private Customer customer;
	@ManyToOne(cascade=CascadeType.REMOVE)
	@JoinColumn(name="loanDetailsId")
	private LoanDetails loanDetails;
	@Column(name="installmentNo")
	private int installmentNo;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="emiDateTime")
	private Date emiDateTime;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="paymentReceiptDate")
	private Date paymentReceiptDate;
	@Column(name="lateFineCharge")
	private BigDecimal lateFineCharge;
	@Column(name="totalAmount")
	private BigDecimal totalAmount;
	@Column(name="emiAmount")
	private BigDecimal emiAmount;

	public EMIPayment() {
		// TODO Auto-generated constructor stub
	}



	public EMIPayment(Customer customer, LoanDetails loanDetails, int installmentNo, Date emiDateTime,
			Date paymentReceiptDate, BigDecimal lateFineCharge, BigDecimal totalAmount,
			BigDecimal emiAmount) {
		super();
		this.customer = customer;
		this.loanDetails = loanDetails;
		this.installmentNo = installmentNo;
		this.emiDateTime = emiDateTime;
		this.paymentReceiptDate = paymentReceiptDate;
		this.lateFineCharge = lateFineCharge;
		this.totalAmount = totalAmount;
		this.emiAmount = emiAmount;
	}


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}
	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	/**
	 * @return the loan
	 */
	public LoanDetails getLoanDetails() {
		return loanDetails;
	}
	/**
	 * @param loanDetails the loan to set
	 */
	public void setLoanDetails(LoanDetails loanDetails) {
		this.loanDetails = loanDetails;
	}
	/**
	 * @return the installmentNo
	 */
	public int getInstallmentNo() {
		return installmentNo;
	}
	/**
	 * @param installmentNo the installmentNo to set
	 */
	public void setInstallmentNo(int installmentNo) {
		this.installmentNo = installmentNo;
	}
	/**
	 * @return the emiDateTime
	 */
	public Date getEmiDateTime() {
		return emiDateTime;
	}
	/**
	 * @param emiDateTime the emiDateTime to set
	 */
	public void setEmiDateTime(Date emiDateTime) {
		this.emiDateTime = emiDateTime;
	}
	/**
	 * @return the paymentReceiptDate
	 */
	public Date getPaymentReceiptDate() {
		return paymentReceiptDate;
	}
	/**
	 * @param paymentReceiptDate the paymentReceiptDate to set
	 */
	public void setPaymentReceiptDate(Date paymentReceiptDate) {
		this.paymentReceiptDate = paymentReceiptDate;
	}
	/**
	 * @return the lateFineCharge
	 */
	public BigDecimal getLateFineCharge() {
		return lateFineCharge;
	}
	/**
	 * @param lateFineCharge the lateFineCharge to set
	 */
	public void setLateFineCharge(BigDecimal lateFineCharge) {
		this.lateFineCharge = lateFineCharge;
	}
	/**
	 * @return the totalAmount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	/**
	 * @return the emiAmount
	 */
	public BigDecimal getEmiAmount() {
		return emiAmount;
	}
	/**
	 * @param emiAmount the emiAmount to set
	 */
	public void setEmiAmount(BigDecimal emiAmount) {
		this.emiAmount = emiAmount;
	}


}
