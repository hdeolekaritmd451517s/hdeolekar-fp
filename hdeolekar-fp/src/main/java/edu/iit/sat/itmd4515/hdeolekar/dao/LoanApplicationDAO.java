/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanApplication;
import edu.iit.sat.itmd4515.hdeolekar.service.JPAWebEntityManager;

/**
 * This class provides persistence features for managing LoanApplication entity.
 * @author Harshal
 * @since Feb 9, 2017
 */
@Stateless
public class LoanApplicationDAO extends JPAWebEntityManager{
	@EJB
	JPAWebEntityManager jpaWebEntityManager;

	public LoanApplicationDAO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * This method returns list of all the loan application.
	 * @return list of all the loan applications.
	 */
	public List<LoanApplication> getAllLoanApplication() {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return em.createQuery("select l from LoanApplication l", LoanApplication.class).getResultList();
	}


	/**
	 * This method returns list of all approved loan applications.
	 * @return list of all approved loan applications.
	 */
	public List<LoanApplication> getAllApprovedLoanApplication() {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return em.createNamedQuery("LoanApplication.findAllApprovedLoanApplication", LoanApplication.class).getResultList();
	}

	/**
	 * This method returns list of all rejected loan applications.
	 * @return list of all rejected loan applications.
	 */
	public List<LoanApplication> getAllRejectedLoanApplication() {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return em.createNamedQuery("LoanApplication.findAllRejectedLoanApplication", LoanApplication.class).getResultList();
	}

	/**
	 * This method returns list of all new loan applications.
	 * @return list of all new loan applications.
	 */
	public List<LoanApplication> getAllPendingLoanApplication() {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return em.createNamedQuery("LoanApplication.findAllPendingLoanApplication", LoanApplication.class).getResultList();
	}

	/**
	 * This method returns list of all cancelled loan applications.
	 * @return list of all cancelled loan applications.
	 */
	public List<LoanApplication> getAllCancelledLoanApplication() {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return em.createNamedQuery("LoanApplication.findAllCancelledLoanApplication", LoanApplication.class).getResultList();
	}

	/**
	 * This method returns loan application by id.
	 * @param loanApplicationId primary key of loan application table
	 * @return loan application object.
	 */
	public LoanApplication getLoanApplicationById(long loanApplicationId) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return (LoanApplication) em.find(LoanApplication.class, loanApplicationId);
	}

	/**
	 * This method returns all the loan applications by customer id.
	 * @param customerId customer id.
	 * @return list of all the loan applications.
	 */
	public List<LoanApplication> getLoanApplicationByCustomerId(long customerId) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return (List<LoanApplication>) em.createQuery("select l from LoanApplication l where l.customer.id=:customerId", LoanApplication.class)
				.setParameter("customerId", customerId).getResultList();
	}

	/**
	 * This method returns list of all the loan application based on customer and loan application id.
	 * @param customerId customer id
	 * @param loanApplicationId loan application id.
	 * @return list of loan applications.
	 */
	public List<LoanApplication> getLoanApplicationByCustomerAndLoanApplicationId(long customerId, long loanApplicationId) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return (List<LoanApplication>) em.createQuery("select l from LoanApplication l where l.customer.id=:customerId and l.id=:loanApplicationId", LoanApplication.class)
				.setParameter("customerId", customerId)
				.setParameter("loanApplicationId", loanApplicationId).getResultList();
	}

	/**
	 * This method inserts loan application in the database table.
	 * @param loanApplication loan application to be persisted.
	 */
	public void persistLoanApplication(LoanApplication loanApplication) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		em.persist(loanApplication);
	}

	/**
	 * This method updates the loan application
	 * @param loanApplication loan application to be updated.
	 */
	public void updateLoanApplication(LoanApplication loanApplication) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		em.merge(loanApplication);
	}


	/**
	 * This method deletes the loan application record.
	 * @param loanApplication loan application to be deleted.
	 */
	public void deleteLoanApplication(LoanApplication loanApplication) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		LoanApplication managedloanApplication = em.merge(loanApplication);
		em.remove(managedloanApplication);
	}

	/**
	 * This method deletes the loan application record based on customer id.
	 * @param customer customer for which loan applications are to be deleted.
	 */
	public void deleteLoanApplicationByCustomer(Customer customer) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();

		List<LoanApplication> loanApplicationList = em.createNamedQuery("LoanApplication.findAllLoanApplicationByCustomer", LoanApplication.class)
		.setParameter("customerId", customer.getId()).getResultList();

		if(loanApplicationList != null || !loanApplicationList.isEmpty()) {
			for (LoanApplication loanApplication : loanApplicationList) {
				System.out.println("Deleting loan application:"+loanApplication.getId());
				em.remove(loanApplication);
			}
		}
	}
}
