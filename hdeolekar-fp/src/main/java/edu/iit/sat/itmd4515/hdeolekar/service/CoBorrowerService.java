/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.service;

import java.util.List;

import edu.iit.sat.itmd4515.hdeolekar.dao.CoBorrowerDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.CoBorrower;

/**
 * This class provides servicess for CoBorrower entity. Any interaction
 * with the persistence context will happens through this layer.
 * @author Harshal
 * @since Feb 13, 2017
 */
public class CoBorrowerService {
	private CoBorrowerDAO coBorrowerDAO;

	/**
	 * @param coBorrowerDAO
	 */
	public CoBorrowerService(CoBorrowerDAO coBorrowerDAO) {
		super();
		this.coBorrowerDAO = coBorrowerDAO;
	}

	/**
	 * Registers the coborrower
	 * @param collateral
	 */
	public void registerCoborrower(CoBorrower collateral){
		coBorrowerDAO.persistCoBorrower(collateral);
	}

	/**
	 * Deletes the coborrower.
	 * @param collateral
	 */
	public void deleteCoborrower(CoBorrower collateral) {
		coBorrowerDAO.removeCoBorrower(collateral);
	}

	/**
	 * Returns list of all the coborrower.
	 * @return
	 */
	public List<CoBorrower> getAllCoBorrower(){
		return coBorrowerDAO.getAllCoBorrower();
	}

	/**
	 * Returns coborrower based on coborrowerd id
	 * @param id co
	 * @return coborrower object.
	 */
	public CoBorrower getCoBorrowerById(long id){
		return coBorrowerDAO.getCoBorrowerById(id);
	}

	/**
	 * Return list of coborrowers by loan application id.
	 * @param loanApplicationId loan application id
	 * @return list of coborrowers.
	 */
	public List<CoBorrower> getCoBorrowerrByLoanApplicationId(long loanApplicationId){
		return coBorrowerDAO.getAllCoBorrowerByLoanApplicationId(loanApplicationId);
	}
}
