/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.model.EMIPayment;
import edu.iit.sat.itmd4515.hdeolekar.service.JPAWebEntityManager;

/**
 * This class is used to manage loan emi payments.
 * @author Harshal
 * @since Apr 25, 2017
 */
@Stateless
public class EMIPaymentDAO extends JPAWebEntityManager {
	@EJB
	JPAWebEntityManager jpaWebEntityManager;

	public EMIPaymentDAO() {
		System.out.println("EMIPaymentDAO init called");
	}

	/**
	 * This method is used to persist emi in emi_payment table.
	 * @param emiPayment EMIPayment to be persisted
	 */
	public void persistEMIPayment(EMIPayment emiPayment){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		em.persist(emiPayment);
	}

	/**
	 * This method is used to deleted emi payment. Currently, no usage exists.
	 * @param emiPayment emi payment to be deleted.
	 */
	public void removeEMIPayment(EMIPayment emiPayment){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		em.remove(emiPayment);
	}

	/**
	 * This method returns list of all the emi payments in the database.
	 * @return list of all the emi payments from the emi_payment table.
	 */
	public List<EMIPayment> getAllEMIPayments(){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		return em.createNamedQuery("EMIPayment.findAll", EMIPayment.class)
				.getResultList();
	}

	/**
	 * This method returns emi payment based on customer Id and loan application Id.
	 * @param customerId customer id for whom emi is to be retrieved.
	 * @param loanApplicationId loan application Id.
	 * @return
	 */
	public List<EMIPayment> getEMIPaymentsByCustomerId(long customerId, long loanApplicationId){
		List<EMIPayment> emiPaymentsList = null;
		try {
			EntityManager em = jpaWebEntityManager.getEntityManager();
			System.out.println("Customer ID:"+ customerId+" Loan Application ID: "+loanApplicationId);
			 emiPaymentsList =  em.createNamedQuery("EMIPayment.findByCustomerId", EMIPayment.class).setParameter("customerId", customerId).setParameter("loanApplicationId", loanApplicationId)
						.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return emiPaymentsList;
	}


	/**
	 * This method returns the list of emi payments based on loan application Id.
	 * @param loanApplicationId loan application Id for which EMI payment is to be retrieved.
	 * @return list of all the emi payments associated with the given loan application id.
	 */
	public List<EMIPayment> getAllEMIPaymentsByLoanApplicationId(long loanApplicationId){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		return em.createNamedQuery("EMIPayment.findByLoanApplicationId", EMIPayment.class)
				.setParameter("loanApplicationId", loanApplicationId)
				.getResultList();
	}



	/**
	 * This method finds customer based on username.
	 * @param username customer username.
	 * @return customer object.
	 */
	public Customer findByUsername(String username) {
		EntityManager em = jpaWebEntityManager.getEntityManager();
		System.out.println("Inside customer dao");
		if (em == null) {
			System.out.println("EntityManager is Null");
		}
		return em.createNamedQuery("Customer.findByUsername", Customer.class)
				.setParameter("username", username).getSingleResult();
	}
}
