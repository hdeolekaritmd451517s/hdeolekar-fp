/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.model;

import java.math.BigDecimal;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * This class represents collateral assets used as a security for the loan application.
 * @author Harshal
 * @since Feb 5, 2017
 */
@Named
@Entity
@Table(name="collateral")
public class Collateral {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotNull
	@Column(name="collateralType")
	@Enumerated(EnumType.STRING)
	private CollateralType collateralType;
	@NotNull
	@Column(name="marketValue")
	private BigDecimal marketValue;

	public Collateral() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param collateralType
	 * @param marketValue
	 */
	public Collateral(CollateralType collateralType, BigDecimal marketValue) {
		super();
		this.collateralType = collateralType;
		this.marketValue = marketValue;
	}


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the collateralType
	 */
	public CollateralType getCollateralType() {
		return collateralType;
	}
	/**
	 * @param collateralType the collateralType to set
	 */
	public void setCollateralType(CollateralType collateralType) {
		this.collateralType = collateralType;
	}
	/**
	 * @return the collateral
	 */
	public CollateralType getCollateral() {
		return collateralType;
	}
	/**
	 * @param collateralType the collateral to set
	 */
	public void setCollateral(CollateralType collateralType) {
		this.collateralType = collateralType;
	}
	/**
	 * @return the marketValue
	 */
	public BigDecimal getMarketValue() {
		return marketValue;
	}
	/**
	 * @param marketValue the marketValue to set
	 */
	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Collateral [id=");
		builder.append(id);
		builder.append(", collateralType=");
		builder.append(collateralType);
		builder.append(", marketValue=");
		builder.append(marketValue);
		builder.append("]");
		return builder.toString();
	}
}
