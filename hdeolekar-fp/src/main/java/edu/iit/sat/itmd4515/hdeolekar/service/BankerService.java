/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import edu.iit.sat.itmd4515.hdeolekar.dao.BankerDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.Banker;

/**
 *The class represents the service layer for Banker class. Any interaction
 * with the persistence context will happen through this layer.
 *
 * @author Harshal
 * @since Apr 5, 2017
 */
@Stateless
public class BankerService {
	@EJB
	private BankerDAO bankerServiceDAO;

	public BankerService() {

	}

	/**
	 * Registers the banker
	 * @param banker
	 */
	public void registerBanker(Banker banker){
		bankerServiceDAO.persistBanker(banker);
	}

	/**
	 * Deletes the banker
	 * @param customer
	 */
	public void deleteBanker(Banker customer) {
		bankerServiceDAO.removeBanker(customer);
	}

	/**
	 * Returns list of banker
	 * @return list of banker
	 */
	public List<Banker> getAllBanker(){
		return bankerServiceDAO.getAllBankers();
	}

	/**
	 * Returns banker by Id
	 * @param bankerId primary key of banker table
	 * @return
	 */
	public Banker getBankerById(long bankerId){
		return bankerServiceDAO.getBankerById(bankerId);
	}

	/**
	 * This method returns banker by username
	 * @param username baker username
	 * @return banker object.
	 */
	public Banker getBankerByUsername(String username){
		System.out.println("Inside customer service");
		return bankerServiceDAO.findByUsername(username);
	}

	/**
	 *
	 * @return the customerServiceDAO
	 */
	public BankerDAO getBankerServiceDAO() {
		return bankerServiceDAO;
	}

	/**
	 * @param bankerServiceDAO the customerServiceDAO to set
	 */
	public void setBankerServiceDAO(BankerDAO bankerServiceDAO) {
		this.bankerServiceDAO = bankerServiceDAO;
	}
}
