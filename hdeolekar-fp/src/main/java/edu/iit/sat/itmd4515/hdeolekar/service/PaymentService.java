/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;

import edu.iit.sat.itmd4515.hdeolekar.dao.EMIPaymentDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.EMIPayment;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanDetails;

/**
 * This class provides services for Customer entity. E.g registering a customer
 * @author Harshal
 * @since Feb 12, 2017
 */
@Named
@Stateless
@RequestScoped
public class PaymentService {
	@EJB
	private EMIPaymentDAO emiPaymentDAO;
	@EJB
	private LoanDetailService loanDetailService;

	public PaymentService() {

	}

	/**
	 * This method inserts emi paid.
	 * @param emiPayment emi paid by customer.
	 */
	public void submitEMIPayment(EMIPayment emiPayment){
		System.out.println("EMIPayment: "+emiPayment);
		emiPaymentDAO.persistEMIPayment(emiPayment);
		System.out.println("EMI Payment Saved");
		LoanDetails loanDetails = emiPayment.getLoanDetails();
		loanDetails.setEmiPaidCount(emiPayment.getLoanDetails().getEmiPaidCount() + 1);
		System.out.println("LoanDetails: "+loanDetails);
		loanDetailService.updateLoanDetails(loanDetails);
		System.out.println("Loan Details Updated");
	}



	/**
	 * This method returns list of all the emi paid by all the customer.
	 * @return list of all the emi
	 */
	public List<EMIPayment> getAllEMIPayments(){
		return  emiPaymentDAO.getAllEMIPayments();
	}

	/**
	 * This method retrieves list of emi paid based on customer and loan details id.
	 * @param customerId customer id
	 * @param loanDetailsId loan details id.
	 * @return list of emi paid.
	 */
	public  List<EMIPayment> getAllEMIPaymentsByCustomerId(long customerId, long loanDetailsId){
		 return emiPaymentDAO.getEMIPaymentsByCustomerId(customerId, loanDetailsId);
	}

	/**
	 * This method retrieves list of emi payment based on loan application id.
	 * @param loanApplicationId
	 * @return
	 */
	public List<EMIPayment> getAllEMIPaymentsByLoanApplicationId(long loanApplicationId){
		 return emiPaymentDAO.getAllEMIPaymentsByLoanApplicationId(loanApplicationId);
	}
}
