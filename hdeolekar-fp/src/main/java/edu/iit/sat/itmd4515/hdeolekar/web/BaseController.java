/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.hdeolekar.web;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

/**
 * This class initializes the faces context for all the controllers
 * @author sas691
 */
public abstract class BaseController{

	protected FacesContext context;

    protected BaseController() {
    }

    @PostConstruct
    public void init(){
    	System.out.println("BaseController Context Initialized");
        context = FacesContext.getCurrentInstance();
    }
}
