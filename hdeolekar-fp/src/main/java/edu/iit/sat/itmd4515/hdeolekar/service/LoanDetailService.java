package edu.iit.sat.itmd4515.hdeolekar.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;

import edu.iit.sat.itmd4515.hdeolekar.dao.LoanDetailDAO;
import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.model.LoanDetails;

/**
 * This class represents service layer for loan details.
 * @author Harshal
 * @since Feb 13, 2017
 */
@Named
@Stateless
@RequestScoped
public class LoanDetailService {
	private LoanDetails loanDetails;

	@EJB
	private LoanDetailDAO loanDetailDAO;

	@EJB
	private CustomerService customerService;

	public LoanDetailService() {
		// TODO Auto-generated constructor stub
	}

	public LoanDetailService(LoanDetailDAO loanDetailDAO) {
		this.loanDetailDAO = loanDetailDAO;
	}

	/**
	 * Returns loan details by loan details id
	 * @param loanId primary key of loan details table.
	 * @return loan details.
	 */
	public LoanDetails getLoanDetailsById(long loanId) {
		LoanDetails loanDetails = loanDetailDAO.getLoanDetailsById(loanId);
		return loanDetails;
	}

	/**
	 * Retrives loan details by loan application id.
	 * @param loanApplicationId loan application id.
	 * @return null to be on the same page.
	 */
	public String getLoanDetailsByLoanApplicationId(long loanApplicationId) {
		FacesContext context = FacesContext.getCurrentInstance();
		if(context.getExternalContext().isUserInRole("CUSTOMER_ROLE")) {
			Customer loggedInCustomer = customerService.getCustomerByUsername(context.getExternalContext().getRemoteUser());
			System.out.println(loggedInCustomer);
			this.loanDetails = loanDetailDAO.getLoanDetailsByCustomerId(loggedInCustomer.getId(), loanApplicationId);
		} else {
			this.loanDetails = loanDetailDAO.getLoanDetailsByLoanApplicationId(loanApplicationId);
		}
		return null;
	}

	/**
	 * Returns loan details by customer id.
	 * @param customerId customer id
	 * @return loan detail records.
	 */
	public LoanDetails getLoanDetailsByCustomerId(long customerId) {
		LoanDetails loanDetails = loanDetailDAO.getLoanDetailsByCustomerId(customerId);
		return loanDetails;
	}

	/**
	 * Updates the loan details.
	 * @param loanDetails loan details to be updated.
	 */
	public void updateLoanDetails(LoanDetails loanDetails) {
		EntityManager em =  loanDetailDAO.getEntityManager();
		em.merge(loanDetails);
	}


	/**
	 * Returns the emi
	 * @param loanApplicationId loan application id
	 * @return emi
	 */
	public BigDecimal getLoanEMIPayment(long loanApplicationId) {
		LoanDetails loanDetails = loanDetailDAO.getLoanDetailsByLoanApplicationId(loanApplicationId);
		// annual interest rate
		float interestRate = (loanDetails.getInterestRate().floatValue() / 100);
		// monthly interest rate
		double monthlyRate = interestRate / 12.0;
		// no of months/payments the EMI has to be paid for total loan
		int termInMonths = loanDetails.getLoanTenure() * 12;
		// monthly installments to be paid.
		double monthlyPayment = (loanDetails.getLoanAmount().intValue() * monthlyRate) / (1 - Math.pow(1 + monthlyRate, -termInMonths));
		return new BigDecimal(monthlyPayment).setScale(2, RoundingMode.CEILING);
	}

	/**
	 * Returns the number of installments.
	 * @param loanApplicationId loan application id.
	 * @return number of installments.
	 */
	public int getNumberOfInstallments(long loanApplicationId){
		LoanDetails loanDetails = loanDetailDAO.getLoanDetailsByLoanApplicationId(loanApplicationId);
		return loanDetails.getLoanTenure() * 12;
	}

	/**
	 * Returns loan tenure.
	 * @param loanApplicationId loan application id
	 * @return loan tenure.
	 */
	public int getLoanTenure(long loanApplicationId){
		LoanDetails loanDetails = loanDetailDAO.getLoanDetailsByLoanApplicationId(loanApplicationId);
		return loanDetails.getLoanTenure();
	}

	/**
	 * This method persists loan details
	 * @param loanDetails loan details to be persisted.
	 */
	public void submitLoanDetails(LoanDetails loanDetails){
		 loanDetailDAO.persistLoanDetails(loanDetails);
	}

	/**
	 * This method deletes loan details.
	 * @param loanDetails loan details to be deleted.
	 */
	public void deleteLoanDetails(LoanDetails loanDetails){
		 loanDetailDAO.deleteLoanDetails(loanDetails);
	}

	/**
	 * This method returns list of details of all the loan application.
	 * @return list of loan details.
	 */
	public List<LoanDetails> getAllLoanApplicationDetails() {
		return loanDetailDAO.getAllLoanDetails();
	}

	/**
	 * @return the loanDetails
	 */
	public LoanDetails getLoanDetails() {
		return loanDetails;
	}

	/**
	 * @param loanDetails the loanDetails to set
	 */
	public void setLoanDetails(LoanDetails loanDetails) {
		this.loanDetails = loanDetails;
	}


	/**
	 * This method calculates payment due date based on loan sanction date.
	 * @return emi payment due date.
	 */
	public Date getPaymentDueDate(){
		Date paymentDueDate = new Date();
		if(this.loanDetails != null) {
			Date sanctionDateTemp = this.loanDetails.getLoanSanctionDate(); // to be replaced
			Calendar sanctionCal = Calendar.getInstance();
			sanctionCal.setTime(sanctionDateTemp);
			int sanctionDate = sanctionCal.get(Calendar.DATE);

			Calendar dueCal = Calendar.getInstance();
			dueCal.setTime(paymentDueDate);
			int month = dueCal.get(Calendar.MONTH);
			int year = dueCal.get(Calendar.YEAR);
			int currentDate = dueCal.get(Calendar.DATE);
			dueCal.clear();
			dueCal.set(year, month, sanctionDate);
			paymentDueDate = dueCal.getTime();
			// payment due date:-
			System.out.println("Calculated Payment Due Date: "+paymentDueDate);
			System.out.println("Current Date: "+currentDate+" Due Date: "+sanctionDate);
			if(currentDate >  sanctionDate) {
				this.loanDetails.setEmiLateCharge(new BigDecimal(25));
			} else {
				this.loanDetails.setEmiLateCharge(new BigDecimal(0));
			}
		}
		return paymentDueDate;
	}

	/**
	 * This method returns late fee
	 * @return late fee.
	 */
	public BigDecimal getLateFee() {
		return this.loanDetails.getEmiLateCharge();
	}
}
