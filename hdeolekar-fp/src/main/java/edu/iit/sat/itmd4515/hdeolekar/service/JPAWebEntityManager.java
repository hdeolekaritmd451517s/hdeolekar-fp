/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * This class provides access to the persistent context by initializing it.
 * @author Harshal
 * @since March 22, 2017
 */

@Stateless
public class JPAWebEntityManager {

	@PersistenceContext(unitName = "itmd4515PU")
	private EntityManager em;

	public JPAWebEntityManager() {
		System.out.println("JPAWebEntityManager init called");
	}

	/**
	 * Returns the entity manager object.
	 * @return
	 */
	public EntityManager getEntityManager(){
		return em;
	}
}
