package edu.iit.sat.itmd4515.hdeolekar.timer;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Stateless;

/**
 * This class represent ebj timer service.
 * @author Harshal
 * @since May 6, 2017
 */
@Singleton
public class TimerService {
    @EJB
    HelloService helloService;

    @Schedule(second="*/50", minute="*",hour="*", persistent=false)
    public void doWork(){

        System.out.println("timer: " + helloService.sayHello());
    }
}
