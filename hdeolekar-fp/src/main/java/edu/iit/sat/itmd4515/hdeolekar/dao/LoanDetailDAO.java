/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import edu.iit.sat.itmd4515.hdeolekar.model.LoanDetails;
import edu.iit.sat.itmd4515.hdeolekar.service.JPAWebEntityManager;

/**
 * This class provides persistence features for managing LoanDetails entity.
 * Currently, details for loan application are all static e.g. interest rates, tenure, only
 * loan amount is variable.
 * @author Harshal
 * @since Feb 9, 2017
 */
@Stateless
public class LoanDetailDAO extends JPAWebEntityManager{
	@EJB
	JPAWebEntityManager jpaWebEntityManager;

	public LoanDetailDAO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * This method returns list of all the loan application detials.
	 * @return list of all the loan application details.
	 */
	public List<LoanDetails> getAllLoanDetails() {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return em.createQuery("select lm from LoanDetails lm", LoanDetails.class).getResultList();
	}

	/**
	 * This method returns loan details by loan application id.
	 * @param loanApplicationId loan application id.
	 * @return Loan details object.
	 */
	public LoanDetails getLoanDetailsByLoanApplicationId(long loanApplicationId) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return (LoanDetails) em.createQuery("select ld from LoanDetails ld where ld.loanApplication.id = :loanApplicationId"
				, LoanDetails.class).setParameter("loanApplicationId", loanApplicationId).getSingleResult();
	}

	/**
	 * This method returns loan details by loan detail id.
	 * @param loanId loan details id.
	 * @return loan detail object.
	 */
	public LoanDetails getLoanDetailsById(long loanId) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return (LoanDetails) em.find(LoanDetails.class, loanId);
	}

	/**
	 * This method returns loan details by customer id.
	 * @param customerId customer id
	 * @return loan details object.
	 */
	public LoanDetails getLoanDetailsByCustomerId(long customerId) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return (LoanDetails) em.find(LoanDetails.class, customerId);
	}

	/**
	 * This method returns loan details by customer and loan application id.
	 * @param customerId customer id
	 * @param loanApplicationId loan application id
	 * @return
	 */
	public LoanDetails getLoanDetailsByCustomerId(long customerId, long loanApplicationId) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		return (LoanDetails) em.createNamedQuery("LoanDetails.findLoanDetailsByCustomerAndLoanApplicationId", LoanDetails.class)
				.setParameter("customerId", customerId).setParameter("loanApplicationId", loanApplicationId).getSingleResult();
	}

	/**
	 * This method persists loan details.
	 * @param loanDetails loan details to be persisted.
	 */
	public void persistLoanDetails(LoanDetails loanDetails) {
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		em.persist(loanDetails);
	}

	/**
	 * This method deletes loan details.
	 * @param loanDetails loan details to be removed.
	 */
	public void deleteLoanDetails(LoanDetails loanDetails){
		EntityManager em =  jpaWebEntityManager.getEntityManager();
		em.remove(loanDetails);
	}
}

