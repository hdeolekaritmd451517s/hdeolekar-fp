package edu.iit.sat.itmd4515.hdeolekar.email;


import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import edu.iit.sat.itmd4515.hdeolekar.model.LoanApplication;

/**
 * This class is used to send appropriate emails to the users when loan is approved by the banker.
 * This is the only functionality for which email is configured.
 * * @author Harshal Deolekar
 * @version 1.0
 * @since December 2, 2016
 */
public class SendHTMLEmail {

    // Recipient's email ID needs to be mentioned.
    private String to;

    // email message.
    private MimeMessage message;

    private static Properties properties = System.getProperties();


    /**
     * Constructor loads all the email settings.
     * @throws AddressException
     * @throws MessagingException
     */
    public SendHTMLEmail() throws AddressException, MessagingException {

        // Setup mail server
        properties.setProperty("mail.transport.protocol", "smtp");
        System.out.println("mail.smtp.host:"+Configuration.get(Configuration.MAIL_SMTP_HOST));
        properties.put("mail.smtp.host", Configuration.get(Configuration.MAIL_SMTP_HOST));
        System.out.println("mail.smtp.socketFactory.port:"+Configuration.get(Configuration.MAIL_SMTP_SOCKETFACTORY_PORT));
        properties.put("mail.smtp.socketFactory.port", Configuration.get(Configuration.MAIL_SMTP_SOCKETFACTORY_PORT));
        System.out.println("mail.smtp.socketFactory.port:"+Configuration.get(Configuration.MAIL_SMTP_SOCKETFACTORY_CLASS));
        properties.put("mail.smtp.socketFactory.port", Configuration.get(Configuration.MAIL_SMTP_SOCKETFACTORY_CLASS));
        System.out.println("mail.smtp.auth:"+Configuration.get(Configuration.MAIL_SMTP_AUTH));
        properties.put("mail.smtp.auth", Configuration.get(Configuration.MAIL_SMTP_AUTH));
        System.out.println("mail.smtp.port:"+Configuration.get(Configuration.MAIL_SMTP_PORT));
        properties.put("mail.smtp.port", Configuration.get(Configuration.MAIL_SMTP_PORT));
        System.out.println("mail.smtp.starttls.enable"+Configuration.get(Configuration.MAIL_SMTP_STARTTLS_FLAG));
        properties.put("mail.smtp.starttls.enable", Configuration.get(Configuration.MAIL_SMTP_STARTTLS_FLAG));

        // creates a valid session by authenticating user.
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
  			protected PasswordAuthentication getPasswordAuthentication() {
  				System.out.println("username:"+Configuration.get(Configuration.MAIL_SMTP_USER)+" password:"+Configuration.get(Configuration.MAIL_SMTP_USER_PASS));
  				return new PasswordAuthentication(Configuration.get(Configuration.MAIL_SMTP_USER), Configuration.get(Configuration.MAIL_SMTP_USER_PASS));
  			}
  		});

        message = new MimeMessage(session);
        message.setFrom(new InternetAddress(Configuration.get(Configuration.MAIL_SMTP_USER)));
	}

	/**
	 * This method sends an email.
	 * @param to email recipient.
	 * @param emailMessage email body/message to be sent.
	 * @throws MessagingException
	 */
	public void sendMessage(String to, String emailMessage) throws MessagingException {
		try {
			message.setSubject("XYZ Financial:- Your Loan Application is Approved!");
			// Set  the header.
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
			// sets content type
			message.setContent(emailMessage, "text/html");
			// Send message
			Transport.send(message);
			System.out.println("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

	/**
	 * This method returns the email html message body.
	 * @param obj Object used for creating email.
	 * @param messageType
	 * @return
	 */
	public String getMessage(Object obj) {
		String message = "";
		LoanApplication loanApplication = (LoanApplication) obj;
		message = "<BODY BGCOLOR='FFFFFF'><strong>Dear " + loanApplication.getCustomer().getFirstName()+" " + loanApplication.getCustomer().getLastName()+ ",</strong><br>"
				+ "I am happy to inform you that XYZ Finance Pvt Ltd has approved your loan application.for"+loanApplication.getLoanAmount()+" "
						+ "As indicated in earlier correspondence, we expect the loan to be paid in full within seven years.An amortization schedule is enclosed."
						+ "Please call me sometime during the week so that we can draw up the final papers.<br>"
				+"Regards,<br>XYZ Financial Pvt Ltd,<br>3300 S Federal St,Chicago, IL - 60616 | (510)-640-6361 <br></BODY>";
		return message;
	}
}