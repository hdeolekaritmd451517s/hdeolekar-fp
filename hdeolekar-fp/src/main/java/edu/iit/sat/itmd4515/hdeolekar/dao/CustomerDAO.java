/**
 *
 */
package edu.iit.sat.itmd4515.hdeolekar.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import edu.iit.sat.itmd4515.hdeolekar.model.Customer;
import edu.iit.sat.itmd4515.hdeolekar.service.JPAWebEntityManager;

/**
 * This class provides persistence features for Customer entity.
 * @author Harshal
 * @since Feb 12, 2017
 */
@Stateless
public class CustomerDAO extends JPAWebEntityManager{
	@EJB
	JPAWebEntityManager jpaWebEntityManager;

	public CustomerDAO() {
		System.out.println("CustomerDAO init called");
	}

	/**
	 * This method updates customer record in the customer table.
	 * @param customer customer record to be updated
	 */
	public void updateCustomer(Customer customer) {
		System.out.println("Updating customer: "+customer);
		EntityManager em = jpaWebEntityManager.getEntityManager();
		em.merge(customer);
	}

	/**
	 * This method inserts customer record in the customer table.
	 * @param customer customer object to be insert in customer table
	 */
	public void persistCustomer(Customer customer){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		em.persist(customer);
	}

	/**
	 * This method deletes customer record from customer table.
	 * @param customer customer object to be deleted
	 */
	public void removeCustomer(Customer customer){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		Customer managedCustomer = em.merge(customer);
		em.remove(managedCustomer);
	}

	/**
	 * This method returns list of all the customers from the customer table.
	 * @return list of customer objects.
	 */
	public List<Customer> getAllCustomers(){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		return em.createNamedQuery("Customer.findAll", Customer.class).getResultList();
	}

	/**
	 * This method returns cutomer record based on customer id.
	 * @param customerId primary key of customer table.
	 * @return customer object
	 */
	public Customer getCustomerById(long customerId){
		EntityManager em = jpaWebEntityManager.getEntityManager();
		return (Customer)em.find(Customer.class, customerId);
	}

	/**
	 * This record returns customer record based on customer username.
	 * @param username customer username
	 * @return customer object.
	 */
	public Customer findByUsername(String username) {
		EntityManager em = jpaWebEntityManager.getEntityManager();
		if (em == null) {
			System.out.println("EntityManager is Null");
		}
		return em.createNamedQuery("Customer.findByUsername", Customer.class)
				.setParameter("username", username).getSingleResult();
	}
}
