insert ignore into sec_group(groupname, groupdesc) values('CUSTOMERS','This group contains customers.');
insert ignore into sec_group(groupname, groupdesc) values('BANKERS','This group contains bankers.');
insert ignore into sec_group(groupname, groupdesc) values('ADMIN','This group contains administrators.');

insert ignore into sec_user(username, password) values('admin', SHA2('admin', 256));
insert ignore into sec_user(username, password) values('customer1', SHA2('customer1', 256));
insert ignore into sec_user(username, password) values('customer2', SHA2('customer2', 256));
insert ignore into sec_user(username, password) values('customer3', SHA2('customer3', 256));
insert ignore into sec_user(username, password) values('banker1', SHA2('banker1', 256));
insert ignore into sec_user(username, password) values('banker2', SHA2('banker2', 256));
insert ignore into sec_user(username, password) values('banker3', SHA2('banker3', 256));
insert ignore into sec_user_groups(username, groupname) values('admin','ADMIN');
insert ignore into sec_user_groups(username, groupname) values('customer1','CUSTOMERS');
insert ignore into sec_user_groups(username, groupname) values('customer2','CUSTOMERS');
insert ignore into sec_user_groups(username, groupname) values('customer3','CUSTOMERS');
insert ignore into sec_user_groups(username, groupname) values('banker1','BANKERS');
insert ignore into sec_user_groups(username, groupname) values('banker2','BANKERS');
insert ignore into sec_user_groups(username, groupname) values('banker3','BANKERS');


insert into customer(id, dateOfBirth, email, firstName, gender, lastName, phone, ssn, username) values (1,'1982-10-06','hdeolekar@hawk.iit.edu','Presza','FEMALE','Hudson','9270363845','145255699', 'customer1');
insert into customer(id, dateOfBirth, email, firstName, gender, lastName, phone, ssn, username) values (2,'1982-10-07','sjesu@hawk.iit.edu','Tracy','FEMALE','Watson','3129759514','124598683', 'customer2');
insert into customer(id, dateOfBirth, email, firstName, gender, lastName, phone, ssn, username) values (3,'1982-10-08','test@yahoo.com','Kevin','MALE','Peterson','7045148414','256994783', 'customer3');

insert  into address(id, address1, address2, city, country, pincode, state) values (1,'450 West 34th St','Apt 708','Chicago','United States','60616','Illinois');
insert  into address(id, address1, address2, city, country, pincode, state) values (2,'250 East 31th St','Apt 709','Chicago','United States','60616','Illinois');
insert  into address(id, address1, address2, city, country, pincode, state) values (3,'350 South 55th St','Apt 710','Chicago','United States','60616','Illinois');

update address set customerId = 1 where id = 1;
update address set customerId = 2 where id = 2;
update address set customerId = 3 where id = 3;

update customer set addressId = 1 where id = 1;
update customer set addressId = 2 where id = 2;
update customer set addressId = 3 where id = 3;

insert  into collateral(id, collateralType, marketValue) values (1,'FLAT','450000'), (2,'FLAT','80000'), (3,'FIXED_DEPOSIT','38000');

insert into banker(id, firstname, lastname, email, phone, username) values(1, 'Zhang', 'Wu', 'zwu@gmail.com', '7034567891', 'banker1');
insert into banker(id, firstname, lastname, email, phone, username) values(2, 'Jack', 'Willey', 'jwilley@gmail.com', '7034567892', 'banker2');


insert  into loan_application(id, EMAIL, LOANAMOUNT, LOANAPPLICATIONDATE, loanType, MONTHLYINCOME, phone, customerId, collateralID) values  (1,'test@yahoo.com','23000','2017-03-13','HOME_LOAN','15000','7045148414', 1, 1), (2,'hdeolekar@hawk.iit.edu','45000','2017-03-14','HOME_LOAN','16000','9270363845', 2, 2), (3,'example@gmail.com','55000','2017-03-15','HOME_LOAN','12000','3129759514', 3, 3);

insert  into loan_master(id, interestRate, loan_amount, emi_amount, emi_count, total_emi_count, applicationDate, sanctionDate, tenure, loanType, customerId, collateralType) values (1,'12','23000', '273', 0, 84, '2017-03-13','2017-04-14',7,'HOME_LOAN', 1, 1), (2,'12','45000', '535', 0, 84, '2017-03-14','2017-04-15',7,'HOME_LOAN', 2, 2), (3,'12','55000', '654', 0, 84, '2017-03-15','2017-04-16',7,'HOME_LOAN', 3, 3);


insert  into emi_payment(id, emiAmount, emiDateTime, installmentNo, lateFineCharge, paymentReceiptDate, totalAmount, customerId) values (1,'1500','2017-03-06 00:00:00',123456,'0','2017-03-13 14:15:33','1500',1);


insert  into coborrower(id, employmentType, firstName, lastName, netMonthlyIncome, relationship) values (1,'SELF_EMPLOYED_PROFESSIONAL','Matt','Johnson','10000','Brother'), (2,'SELF_EMPLOYED_BUSINESS','Jean','Hudson','20000','Sister'), (3,'SELF_EMPLOYED_PROFESSIONAL','Mark','Henry','15000','Brother');

update loan_application set loanDetailsId = 1 where id = 1;
update loan_application set loanDetailsId = 2 where id = 2;
update loan_application set loanDetailsId = 3 where id = 3;

update loan_master set loanApplicationId = 1 where id = 1;
update loan_master set loanApplicationId = 2 where id = 2;
update loan_master set loanApplicationId = 3 where id = 3;

update coborrower set loanDetailsId = 1 where id = 1;
update coborrower set loanDetailsId = 2 where id = 2;
update coborrower set loanDetailsId = 3 where id = 3;

update emi_payment set loanDetailsId = 1 where id = 1;
update emi_payment set loanDetailsId = 2 where id = 2;
update emi_payment set loanDetailsId = 3 where id = 3;

-- update coborrower set loanAppId = 1 , loanDetailsId = 1 where id = 1;
-- update coborrower set loanAppId = 2 , loanDetailsId = 2 where id = 2;
-- update coborrower set loanAppId = 3 , loanDetailsId = 3 where id = 3;

insert  into loan_coborrower(loanDetailsId, coborrowerId) values (1,1),(2,2),(3,3);

insert  into loan_app_coborrower(loapAppId, coborrowerId) values (1,1),(2,2),(3,3);
